name = Thesis
dict = mywords.utf-8.add

all: $(name).pdf $(name).tgz

$(name).pdf: tex/$(name).tex tex/*.tex MyLibrary.bib img/*
	lualatex -interaction=batchmode -shell-escape $<
	bibtex $(name)
	lualatex -interaction=batchmode -shell-escape $<
	lualatex -shell-escape $<

Short.pdf: short/Short.tex tex/*.tex short/*.tex
	latexmk -pdf -interaction=batchmode -shell-escape $<

once: tex/$(name).tex tex/*.tex MyLibrary.bib
	lualatex -shell-escape $<

$(dict): ci/my_words.txt tex/abbreviations.tex
	cp ci/my_words.txt $@
	cat tex/abbreviations.tex | sed -nE 's/\\addac\{(.*)\}\{.*/\1/p' >> $@
	cat tex/abbreviations.tex | sed -nE 's/\\addac\{(.*)\}\{.*/\1s/p' >> $@
	cp $@ ci/vale_styles/accept.txt

$(dict).spl: $(dict)
	vim "+mkspell! $@ $^" +qall

vimrc: $(dict) $(dict).spl
	echo "set spelllang=en_gb" > .vimrc
	echo "set spellfile=$(dict)" >> .vimrc
	echo "set wrap linebreak nolist" >> .vimrc
	echo "set textwidth=79" >> .vimrc

clean:
	$(RM) $(name).pdf Short.pdf *.o *.log *.aux *.bbl *.log *.toc *.out tex/*.aux short/*.aux *.blg

$(name).tgz : Makefile tex/*.tex $(name).pdf
	tar --file $@ --create --gzip $^
	@echo "have just archived the following files"
	@tar --file $@ --list

AssessmentResponse.docx : Assessment.md
	pandoc $< -f markdown -s -o $@
