
# echo "Downloading plagiarism checker"
# curl -sSfL -o plag_checker https://github.com/frizensami/plagiarism-basic/releases/download/v1.0.0/plagiarism-basic-v1_0_0-x86_64-unknown-linux-gnu
# chmod +x ./plag_checker
# ls -l ./plag*
# /bin/sh -c "./plag_checker -V"

echo "Downloading plagiarism checkers assets"
mkdir -p /home/sriram/Projects/plagiarism-basic/plagiarismbasic_lib/templates
curl -sSfL -o report.hbs https://raw.githubusercontent.com/frizensami/plagiarism-basic/master/plagiarismbasic_lib/templates/report.hbs
mv report.hbs /home/sriram/Projects/plagiarism-basic/plagiarismbasic_lib/templates/
mkdir -p /home/sriram/Projects/plagiarism-basic/plagiarismbasic_lib/assets
curl -sSfL -o jquery.min.js https://raw.githubusercontent.com/frizensami/plagiarism-basic/master/plagiarismbasic_lib/assets/jquery.min.js
curl -sSfL -o semantic.min.js https://raw.githubusercontent.com/frizensami/plagiarism-basic/master/plagiarismbasic_lib/assets/semantic.min.js
mv *.js /home/sriram/Projects/plagiarism-basic/plagiarismbasic_lib/assets/
curl -sSfL -o semantic.min.css https://raw.githubusercontent.com/frizensami/plagiarism-basic/master/plagiarismbasic_lib/assets/semantic.min.css
curl -sSfL -o styles.css https://raw.githubusercontent.com/frizensami/plagiarism-basic/master/plagiarismbasic_lib/assets/styles.css
mv *.css /home/sriram/Projects/plagiarism-basic/plagiarismbasic_lib/assets/


echo "Downloading Master Thesis"
curl -sSfL -o MasterThesis.tar.gz https://gitlab.com/agravgaard/MastersThesis/-/archive/master/MastersThesis-master.tar.gz
tar -xf MasterThesis.tar.gz

echo "Removing non-text files"
rm tex/Thesis.tex tex/contents.tex tex/publist.tex tex/abbreviations.tex
rm MastersThesis-master/tex/Thesis.tex MastersThesis-master/tex/contents.tex MastersThesis-master/tex/publist.tex MastersThesis-master/tex/abbreviations.tex

echo "Plagiarism checker running..."
plagcheck -t ./MastersThesis-master/tex -u ./tex -m equal -n 15 -s 0 --html || ls
mv www public

