FROM alpine:3.14 AS builder

WORKDIR /home/user

RUN apk update && \
    apk add make gcc musl-dev flex-dev git && \
    git clone https://github.com/pkubowicz/opendetex.git && \
    cd opendetex && \
    make

FROM alpine:3.14 AS final

COPY --from=builder /home/user/opendetex/detex /detex

ENTRYPOINT ["/detex"]
