ARG check

FROM rust:1.49 AS builder

RUN cargo install plagiarism-basic

COPY ./plagcheck_docker/* /tmp/

WORKDIR /tmp/

RUN mkdir -p /home/sriram/Projects/plagiarism-basic/plagiarismbasic_lib/templates \
    && mv report.hbs /home/sriram/Projects/plagiarism-basic/plagiarismbasic_lib/templates/

RUN mkdir -p /home/sriram/Projects/plagiarism-basic/plagiarismbasic_lib/assets \
    && mv *.js /home/sriram/Projects/plagiarism-basic/plagiarismbasic_lib/assets/ \
    && mv *.css /home/sriram/Projects/plagiarism-basic/plagiarismbasic_lib/assets/

WORKDIR /

FROM builder AS plag

RUN curl -SL https://gitlab.com/agravgaard/MastersThesis/-/archive/master/MastersThesis-master.tar.gz \
    | tar -xz \
    && mv MastersThesis-master other \
    && rm other/tex/Thesis.tex other/tex/contents.tex other/tex/publist.tex other/tex/abbreviations.tex

FROM builder AS diff

RUN curl -SL https://gitlab.com/agravgaard/phd-thesis/-/archive/1.0.0/phd-thesis-1.0.0.tar.gz \
    | tar -xz \
    && mv phd-thesis-1.0.0 other \
    && rm other/tex/Thesis.tex other/tex/contents.tex other/tex/publist.tex other/tex/abbreviations.tex

FROM ${check} as final

ENTRYPOINT ["/usr/local/cargo/bin/plagiarism-basic", "-t", "/other/tex", "-u", "/tex", "-m", "equal", "-n", "15", "-s", "0", "--html"]
