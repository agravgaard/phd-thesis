Received Date: August 19, 2021

Assessment of the PhD thesis submitted to The Graduate School of Health, Aarhus University entitled:

“Strategies for evaluating and improving proton plan robustness towards inter-fractional organ motion”

by

Andreas Gravgaard Andersen

The assessment committee, appointed on May 4, 2021, by the Graduate School of Health comprised the following members:
```
Professor Michael R. Horsman (Chairman of the committee)
Institute of Clinical Medicine
Experimental Clinical Oncology-Dept. Oncology
Aarhus University Hospital and Aarhus University
Palle Juul-Jensens Blvd. 99
DK-8200 Aarhus N
Denmark

Prof. Dr., Ph.D., Katia Parodi (Assessor 1)
Department of Experimental Physics – Medical Physics
Ludwig-Maximilians-Universität München
Am Coulombwall 1
85748 Garching, München
Germany

M.D., Ph.D., Peter Meidahl Petersen (Assessor 2)
Department of Oncology
The Finsen Centre, Section 5073
Copenhagen University Hospital
Rigshospitalet
Blegdamsvej 9
DK-2100 Copenhagen Ø
Denmark

Prof. Ludvig Paul Muren (Main Supervisor and non-voting member of the committee)
Danish Centre for Particle Therapy
Department of Clinical Medicine
Aarhus University and Aarhus University Hospital
Palle Juul-Jensens Blvd. 99
DK-8200 Aarhus N
Denmark
```


# Assessment
## Content
The dissertation comprises a review of 34 typed pages with 12 figures, 2 tables, 82 references, and 4 papers. Of these, the Ph.D. student is first author on two published papers, second author on one published paper and shared first author on one submitted paper.

## Evaluation of co-author declarations
 - Paper I: The Ph.D. student has, according to the declaration form, contributed considerably to the concept and design of the study, done almost all the acquisition/analyses, and most of the writing and submission of the paper.
 - Paper II: The Ph.D. student has, according to the declaration form, contributed considerably to the concept and design of the study, contributed considerably to the acquisition/analyses, but less to the writing and submission of the paper.
 - Paper III: The Ph.D. student has, according to the declaration form, contributed considerably to the concept and design of the study, done all the work on acquisition/analyses, and most of the writing and submission of the paper.
 - Paper IV: The Ph.D. student has, according to the declaration form, contributed considerably to the concept and design of the study, contributed considerably to the acquisition/analyses, and most of the writing and submission of the paper.

 - [x] The Ph.D. student clearly states that papers II and IV were included in an earlier thesis by Kia Busch. A detailed explanation (see attached document) was supplied by the student (and main supervisor) to demonstrate that the contributions of each student were different and thus justifying inclusion in two Ph.D. theses. However, there was no explanation of how the data was used differently in the two theses. The percentage contribution between the Ph.D. candidate and Kia Busch to papers II and IV certainly do not overlap (i.e., if one contributed 67-90% the other contributed 34-66%) for all categories except one; for the submission process including revisions for paper IV both contributed 90%, which seems unlikely.
   * **Kia Busch expected to handle the submission of Paper IV when handing in her dissertation, but as she was busy at the time. I did it instead. Currently, the contribution share is closer to 70(me)-30(Kia Busch) for the revisions.**

## Scope
 - [x] The dissertation states that the thesis will focus on the changes in the scope of inter-fractional radiotherapy, (i.e., anatomical changes affecting the treatment). The overall background for doing this thesis and the overall aim is not described in more detail. The aim of the individual papers are given as described below. The target audience is not stated but it appears that the thesis cannot be intended for physicians or clinicians. The clinical content is very limited and the physics is not adequately explained for clinicians. Although this thesis should have been more directed to medical physicists than physicians, the audience of the thesis should be better identified to be able to provide proper guidance and judgement.
   * **"This Thesis is intended for medical physics researchers. The intention is also to provide enough information for further developing the tools and methods of this thesis." This paragraph has been added to the beginning of the background chapter.**
   * **Furthermore, the general readability of the thesis has been improved. Long sentences were split. Complicated words have been replaced, removed or better explained.**
 - [x] The boundaries of the study are not defined as a precise explanation for the rationale to do this thesis and the choices of methods and materials are not given. Essentially, the thesis is based on 8-11 scans in 18 patients in paper I, planning on 8-9 scans in 8 patients in papers II + IV and a number of scans of phantoms in paper III. The Ph.D. student is first author of papers I + III.  The methods used were, for the most part, already existing methods, so no major new methodology was developed. The more original parts of the work are probably related to the implementation and further development of a previously published scatter correction method of CBCT, used in this thesis for the first time on a system mounted at a proton gantry, and the proposal of new metrics to evaluate the role of WEPL calculations as pre-treatment control of the possible dose degradation based on inter-fractional anatomical changes.
   * **I hope I have addressed the missing explanation for the rationale for the thesis and choices of methods and materials sufficiently through the more specific comments below.**
 - [x] The assessment committee felt an unbalance between the extremely detailed description of for example computer tools (e.g., section 3.6) while a more superficial introduction to the context of the thesis and to the basics of proton therapy. For example, prostate cancer is a rather controversial indication for proton therapy, hence the main focus of this thesis on this particular indication (which may become more evident when reading the discussion of paper IV) could have been better highlighted in the introductory session.
   * **This is one of the main differences between the work of Kia Busch and I. Kia Busch focused on prostate cancer treatment. In contrast, I focused on developing the methods and tools from a more general perspective. I dreamed that these tools should be useable for any anatomical site. Sadly, I never got access to data sets from other patient groups. (Until 15 days before handing in the dissertation. At that time, I was approved for an H&N data set).**
   * **I know this is not well reflected in the thesis, except in the lack of mention of prostate cancer in the overarching aim.**
 - [x] In the introductory part, some biological concepts seem to be quite superficially introduced and sometimes also mixed up between protons and heavier ions (only generically referring to particle therapy). Moreover, the thesis falls quite short in introducing the reader to the main topics which are needed to provide a better understanding of the context and of the work. For example, for proton therapy only the Bethe Bloch formula is introduced (which by the way holds only above ~ 1 MeV), and the description of beam scanning and passive beam delivery is very qualitative and would have largely benefited from some schematic illustrations and more detailed descriptions.
   * **Deep knowledge of these particular topics is not necessary for understanding the rest of the thesis, thus the superficial explanation.**
   * **Particle therapy is used when the difference from Proton therapy is insignificant, or the statement applies to both. Generally, most of the work or studies on proton therapy not related to RBE would also apply to heavier ions.**
   * **An example beamline schematic has been added**
 - [x] Similarly, the difference between fan beam and cone beam CT, and some more details about image reconstruction (which is not always a simple back projection as stated in a few places of the thesis) and examples of image quality could have been given more thoroughly.
   * **Fan beam is not central to the thesis. Examples of image artefacts is a good idea. I have added an image to the background.**
 - [x] Given the relevance of treatment planning, the concepts of single field uniform dose, IMPT, and robust optimization should be properly reviewed to better appreciate the techniques used in the different studies.
   * **An additional two paragraphs have been added to treatment planning**
 - [x] Also, relevant concepts like NTCP, gEUD and TD50 should be introduced in more depth.
   * **The section on NTCP has been expanded**
 - [x] Moreover, there seem to be some incorrect statements (e.g., “The scatter in CT and CBCT scans are primarily from Compton scattering, and to a lesser degree from the photoelectric effect”) and sentences which would greatly benefit from a revision.
   * **For lower energies, as may be used for CT, photoelectric will be increasingly dominant. However, the energies used in this thesis is in the order of 70-120 keV. To be precise, the statement has been rewritten: "The scatter in CT and CBCT scans are primarily from Compton scattering and the photoelectric effect. Compton scattering dominates at higher energies. The photoelectric effect dominates at lower energies."**
 - [x] Due to the extension of these possible corrections, a version with comments highlighted in the thesis is provided as a separate document, rather than putting all these comments in this report.
   * **Thanks!**
   * **Minor corrections has been performed according to the comments in the document.**
   * **I have added a footnote regarding the z-axis for spots as it is not part of the DICOM standard.**
   * **The alternative energy selection method was rewritten, as I could not find where the system I described was used. (Despite this being taught to me as the primary method in an accelerator course at the university).**
   * **A figure was added to explain the mass attenuation coefficients vs energy relationship for the different interactions.**
   * **OR has been substisuted for OAR throughout the Thesis (OR had been used by my supervisor in the past in other documents - I have no preference)**
   * **d(Ebeam, z) is called "the energy loss distribution" in the reference Krämer et al. 2000 (DOI: 10.1088/0031-9155/45/11/313), so I will stick with that.**
   * **Regarding T describing species: From Krämer et al.: "Although focused on and tested for 12C ions it can be extended to other ion species as well." and "where T represents a particular particle species (Z, A) defined by the nuclear charge Z and the mass number A"**
   * **Nice catch on the LN in the abstract (The aims was missing the LaTeX annotations I use for abbreviations)**
   * **gPMC vs goPMC at the end of the discussion: Botas et al. used gPMC (the Nvidia CUDA version). goPMC is mainly used at UT Southwestern.**
 - [x] Finally, it is felt that some reassurance on the validity of the results should be given after the sentence of page 24 “this bug remains unsolved”.
   * **A better explanation has been added. The MC dose calculation engine is not used in any of the studies. Nevertheless, the bug does not distort data; it hard-crashes the application - no data.**

## The Review
 - Is the discussion of the literature satisfactory?
 - [x] The discussion of the literature in the discussion section of the thesis is very brief and not very clear. As far as techniques for scatter corrections in CBCT and calculation of WEPL as a surrogate for dose monitoring are concerned, the author has listed the more relevant works from the literature, though some more details on the claimed originality of his own approach would have been beneficial.
   * **I have improved the readability of the discussion. This should have improved clarity as well.**
 - Does the dissertation include a clearly formulated hypothesis, which was relevant at the time the PhD project commenced in the light of the available knowledge in the research field in question?
 - [x] There was not a very clear hypothesis overall. In the individual papers the aims are given, however no precise hypotheses are tested. However, the studies are to some extent exploratory, which make an exact hypothesis testing difficult. As mentioned earlier, a better motivation to the focus of this work on inter-fractional anatomical changes, with emphasis on advanced prostate cancer, could have been given. Otherwise, the individual aims are properly given, though also this part could have been slightly expanded to make also a clearer connection among the several studies.
   * **Thanks for acknowledging the exploratory nature of our studies. My goal was not to focus specifically on prostate cancer (Prostate cancer was the focus of the thesis by Kia Busch). Although, I was not able to obtain other complete data sets of patients. A paragraph has been added to make this clearer in the discussion.**
 - Have the considerations in regard to choice of method(s) been clarified, including a presentation of the methodology used, which should reflect a good understanding of the applied methodology, and a critical evaluation of the choice of method?
 - [x] The choice of material and methods are unclear and the physics methods are not explained sufficiently for physicians. Also from a physicist perspective, the introduction would have benefited from some more detailed descriptions of key elements of the work (e.g., treatment planning, beam delivery and planning strategies, CT vs CBCT…) which are in several parts only very superficially introduced (assuming the reader should be already an expert in the field).
   * **A note on the target audience has been added. The relevant sections have been expanded.**
   * **Furthermore, the general readability of the thesis has been improved. Long sentences were split. Complicated words have been replaced, removed or better explained.**
 - Have the results been critically interpreted and has relevant knowledge in the field been included to a sufficient extent in the interpretation?
 - [x] The results in the review are very briefly described and the discussion section is rather short. The text is in major parts of the thesis incomprehensible, particularly for non-experts, so it is difficult to tell if the results have been critically interpreted and if the Ph.D. student has demonstrated relevant knowledge in the field to a sufficient extent. As far as the topic of scatter correction of CBCT is concerned, the author demonstrated broad knowledge of the field and properly discussed the results obtained and limitations of the study, focused only on phantom data. Also, for the topic of range-guidance using WEPL, the candidate showed to have a good overview of the work of other groups and could reasonably hint on the originality of the pursued approach, mainly relying on the new metric used to interpret the WEPL calculations. However, when highlighting the strength of his approach to avoid the issue of re-delineation, he could also properly address the considerable progress done in this field thanks to the recent adoption of Artificial Intelligence (AI) methods, with first implementations of auto-contouring available by vendors especially in the context of photon therapy.
   * **The readability and grammar have been improved. A paragraph on ML-based delineation has been added.**
 - Does the student master relevant terminology and is the wording unambiguous?
 - [x] The thesis has some examples indicating that the Ph.D. student does not use the clinical terminology correctly. Also in other parts there were some inaccuracies in the wording (e.g., “Thirdly, there is the option of having a fixed beam aperture (usually a horizontal beam)…” where the term aperture in proton therapy typically refers to the patient specific collimator in passively scattered proton therapy, while the author probably refers to a fixed beamline.
   * **This specific "inaccuracy" has been fixed as well as any mentioned in other comments.**
 - Is the summary comprehensible?
 - [x] The summary is very concise but reflects the thesis reasonably.
 - Is the review satisfactory overall?
 - [x] We do not find the review satisfactory. In addition, the review does not form a logical part of the dissertation as a whole. The thesis review cannot be interpreted without the papers and does not give a satisfactory overview. The perspectives of the findings are deemed not too well explained. The use of language is often poor. The figures are, in general, unhelpful and very sparse. The review should be more comprehensive, enabling the reader to follow the context and main achievements of the thesis without requiring too much prior knowledge, or having to read many more papers referenced in the text.
   * **This has been addressed in other comments. Additionally, a new figure has been made, and another has been altered based on the suggestions.**
 - Consideration must be taken as to whether the review forms a logical part of the dissertation as a whole.
 - [x] The major parts of the background section and parts of the methods section do not form a logical part of the dissertation. The results are not presented in a way which make the dissertation readable on its own. The writing is deficient, rendering many sections of the dissertation unintelligible, especially for a non-expert.
   * **This was addressed in other comments.**

## Review of the dissertation review (step by step).

### Background
 - [x] The first two paragraphs in section 1.1 about cancer seems out of the scope for this manuscript. The following paragraphs about cancer treatment contain a little bit of relevant information about radiation therapy in cancer, however not at a high academic level.  The background does not have any information about prostate cancer radiotherapy.
   * **As mentioned, the biology of cancer, including prostate cancer, is out of scope for this thesis. Thus, the brevity of this section. It is written with researchers in mind, particularly those who would carry the project in the future. The section has been expanded and clarified according to the comments in the separate document.**
 - [x] Section 1.2 describes very briefly the very basics of photon radiation without any referrals to literature.
   * **Two references have been added to the "Photon therapy" section.**
 - [x] Section 1.3 describes basic aspects about proton therapy (PT) and describes particle interactions. This information is not put into a context in the scope of this thesis.
   * **This section was expanded at the request of my supervisor. Indeed, it is not necessary to understand the rest of the thesis. However, one can say the same for most sections in the background, so I do not see any harm in keeping it.**
 - [x] It is stated that PT takes advantage of spread-out Bragg Peak (SOBP), but it is not explained in relation to anatomical variation.
   * **I do not think it makes sense to talk about anatomical variation before treatment planning has been described.**
 - [x] It is stated that PT is advantageous without any references.
   * **Maybe referring to "children's case", two references have been added, Dinh 2012 and Langendijk 2013.**
 - [x] A paragraph on particle interaction seems out of context.
   * **See the comment above about section 1.3**
 - [x] A section about PT technology describes some features about passive scatter and pencil beam scanning without referring to the aspects related to the scope of this thesis.
   * **The relation is not that strong, but I consider it more important to understand statements later in the thesis, e.g., the GPU MC dose calculation.**
 - [x] The efficacy paragraph doesn’t mention prostate cancer.
   * **My aim for the thesis was not to be prostate-cancer centric but to introduce general methods and apply them to prostate cancer patients as an example.**
 - [x] Section 1.4 about CT could potentially be relevant to the projects using CBCT for replanning, but the aspects of replanning using CBCT is not reviewed. It is believed that the very short paragraph about scatter correction is relevant for the use of CT for replanning, but the relevance for replanning is not given.
   * **We do not use "replanning" in any of the studies. Re-planning is not the subject of this thesis and has not been investigated by us. Although, as referenced for comparison in discussions, others have investigated it, e.g. Botas et al.. They just assumed the same scatter correction method to be sufficiently accurate.**
 - [x] The section is poorly written for a non-expert or more clinical, less medical physics readership, and one sentence does not lead to the next, which makes the message of each sentence very hard to understand, for example, the Ph.D. student states “However, for protons, even the correctness of a CT image is questionable when calculating stopping power. However, for the papers in this thesis, it is assumed that the CT’s electron density map is directly transferable to the stopping power of protons [20]; and the transformation to stopping power is handled through a simple Look-Up Table (LUT)”. Ref 20 conclusion: “We have presented an easily implemented, practical algorithm for tomographic reconstruction from 2D projection data and have illustrated its application to numerically generated projection data. Its performance is shown to be generally comparable with that of the standard fan-beam algorithm, of which it may be regarded as a natural extension.” So, the reference does not exactly support the statement above and the assumption needs an explanation.
   * **I think you may have opened the wrong paper. Reference 20 is coded as "Schaffner1998" in my LaTex file, which links correctly to Schaffner & Pedroni 1998, DOI: 10.1088/0031-9155/43/6/016. Schaffner et al. have the conclusion: "we get an overall approximation of the range error caused by CT and calibration curve of 1.8% and 1.1% for bone and soft tissue respectively.". The conclusion you refer to looks like the one from the Feldkamp, Davis & Kress (FDK) paper, i.e. reference 21.**
 - [x] For a non-expert please explain LUT.
   * **A short elaboration has been added since a look-up table is one of the simplest data structures (with its own Wikipedia entry): "[..] handled through linear interpolation of a simple Look-up table (LUT), i.e. a HU (key), stopping power (value) key-value table."**
 - [x] The paragraph about scatter projection does not help us understand why scatter correction is important and how the correction is done.
   * **The first sentence describes why: "Dose-guidance requires the HU values to be reliable, particularly for PT. With the high amount of scattering in CB projections causing artefacts in the reconstructed CBCT, the HU values are unreliable". In combination with the previous section about CT and CBCT and how HU is used. How the correction is done is described in more detail in the methods chapter for the specifics of this thesis, Figure 6 outlines the algorithm.**
 - [x] Figures 5 and 6 are not very helpful.
   * **I found these two figures extremely useful to understand FDK and the a priori scatter correction. I refer to them because I hope they would also be helpful to someone similar to me taking over the project.**
 - [x] Section 1.5, volume of interest describes the very basic nomenclature. The text has some errors, for example, that setup is a part of ITV.
   * **It seems to me that the ITV is ill-defined (thus the "supposed" in my description) and used differently in different sites of the body and different hospitals. I have added setup uncertainty to the text.**
 - [x] The robust target volume is not explained.  The treatment planning paragraph gives an example of an equation used in the planning system, without explaining why this example is shown and how the equation is used to create a plan.
   * **RTV is explained as "An RTV is similar to a PTV but is generated by the TPS for each field using uncertainty parameters". I cannot describe it further since it is TPS-implementation-defined how the uncertainty parameters are used and mostly proprietary information.**
 - [x] Section 1.6 dose evaluation describes dose evaluation very briefly/superficially and is neither accurate nor useful.
   * **It introduces DVH and NTCP, which is used several times later in the thesis. I do not see the problem.**
 - [x] In section 1.7, treatment verification, the paragraphs about image-and dose guidance are basic and superficial in content and are neither accurate nor very helpful.
   * **The Image and Dose verification methods are specific to the centre (i.e. available equipment, budget and treatment philosophy) and anatomical site. This section does not aim to be comprehensive but rather to give an idea of what image- and dose-guidance may include. But since I only have experience from very few departments and brief descriptions in various papers on the topic, I do not feel confident attempting to specify this section further.**
 - [x] The paragraph about GPU-based proton dose calculation introduces the concept of Graphics Processing Unit without explaining how this unit works.
   * **Describing GPU architecture depends strongly on the specific GPU. The main common thing between GPU's is that they work on highly parallelizable tasks, as already stated.**
 - [x] The last paragraph about beam angle robustness does not give any information.  The whole section is poorly written.
   * **A paragraph has been added to give more information: "Beam angle robustness is a treatment strategy that takes advantage of the angular and spatial distribution of anatomical changes relative to the tumour to potentially find beam directions that would see less change either intra- or inter-fractionally."**

### Aims
 - [x] The aims are described for each paper, however not very precisely and the substandard language may be a reason for this ambiguity.
   * **With the help of "Grammarly" and "Hemingwayapp", I have improved the language, i.e. readability and grammar of the Aims.**

### Material and Methods section
 - [x] Patients: pCT + 7-8 repeat CT scans from 18 pt’s in studies 1 and 8 patients in studies 2 + 4. The text does not mention how the cases were selected. Two phantoms were used, and the methods section does not clarify why the phantoms used were selected for this study or why two different phantoms were used.
   * **The 18 patients were all the patients that had data-sets complete enough for automating the process. Paper II and IV included a much more manual process, and fewer patients were used for this reason. No particular care was taken in the choice, except some patients may have been excluded due to technical difficulties (unreadable/eclipse-incompatible/corrupt data). "The eight patients were selected at random, with exclusion only based on technical difficulties, i.e. incomplete, corrupt, unreadable or otherwise software-incompatible data." has been added for clarification**
 - [x] Following this, a section about Xim Projection file format is presented. It is difficult to understand this section and its relevance to the study.
   * **The projection reader written for this project had a significant impact on reducing the time required for performing the scatter correction. Optimisation and generalisation of the application were central to the originality of the project. In the thesis, we wrote: "The Matlab script took 30 seconds to read just one projection. Luckily, through the power of bitwise operations in C++ and some aggressive optimizations the reading time could be brought down to 2-3 seconds for 500 projection." and later in the discussion "The scatter correction application developed for Paper III is a powerful tool that has been extended in several ways. [...] The application has been optimized and automated so that it can be used in a clinical setting."**
 - [x] The water equivalent path length (WEPL) method section does not explain the principles for the WEPL calculation and gives no explanation for the choice of WEPL as the measure for robustness.
   * **A subsection called "Water equivalent path length" has been added to the "Treatment verification" section.**
 - [x] Incorrect terms are used regarding positions in the body (inferior, proximal, distal….).
   * **"Inferior" does not appear in the Material and Methods section.**
   * **Proximal and distal does not necessarily refer to "positions in the body":**
   * **"Proximal" is used as defined "nearest the point of attachment or origin".**
   * **"Distal" is used as defined "situated away from the point of attachment or origin".**

### Treatment planning and dose calculation section
 - [x] Plans in paper I were single field and no organs at risk (OARs) were considered in the optimization? Does that make sense when the benefit of PT should be a lower dose to OARs, as the target usually can be well covered with photons? The OARs are contoured anyway. The text states that a simple beam model was used due to a large amount of data. This argument does not seem to be valid.
   * **If you have _one_ field and you add OARs to the optimization, what is the expected oucome? If they have a lower priority than the target, hopefully, nothing. Similar or higher priority would mean we would start sacrificing target coverage.**
 - [x] Plans in paper II were made with 90 + 270 degrees and 35 + 325 from paper I. Does this fit with the results: “Eleven out of the 18 patients investigated in this study showed more considerable variations in WEPL (SD > 2:5 mm), peaking at the lateral angles [80; 100]. In contrast, the remaining seven patients did not have any clear pattern in the WEPL maps.”
   * **Yes. 90+270 was the (unfounded) convention we were challenging with the 35+325 configuration. Furthermore, the WEPL results were not used for concluding on robust gantry angles, only couch angles. The gantry angle robustness was mainly determined by the angles with lower OAR doses (the target doses were quite stable for a margin >= 5mm) with fewer outliers throughout treatment.**
 - [x] The scatter correction method section describes the technical steps for this procedure. More knowledge on the methods behind this is necessary in order to assess this section. These methods are not new, so the Ph.D. student has worked on a lot these procedures, using already established procedures.
   * **If I am reading this correctly, no changes are required**

### Statistics section
 - [x] The statistics section mentions patient clustering and refers to reference 47 which does not clearly report on k-nearest-neighbor analysis. Gamma analysis is not explained to clinicians. Correlation with dose metrics given without references.
   * **An additional reference to "Numerical Ecology" has been added, which explains clustering in detail. In particular, I used the heatmap.2 function from the gplot R package with hclust=complete and dist=pearson. However, if I have to start listing all functions I have used throughout the project, the list would probably be hundreds of pages long and not very interesting.**

### Tools section
 - [x] As previously mentioned, this part on the tools is very detailed, and not understandable for a reader not familiar with these programming languages and software tools. At least there seems to be an unbalance between the level of details in this part, and the previous parts where relevant concepts were introduced quite superficially.
   * **To me, this part is the core of the project, the technical work. I.e. for others to be able to take the project further in the future or for people working on similar projects, the information in this section may be crucial.**
 - [x] The last part on the persisting bug would deserve some more attention on giving some confidence on the validity of the results for the related investigations.
   * **No related investigations are presented in this thesis. Furthermore, the bug is in goPMC, which I do not have the source code for and thus no way of correcting.**

### Results
 - [x] Paper I: The text is incoherent. After several attempts we found the text unintelligible. Figure 8 is not helpful, made further complicated by inversing right/left and mentioning the corners of the world. The different circles with different distance to the center are not explained.
   * **I have flipped the figure and replaced the axes with angle markers. Additionally, the figure text has been updated to reflect the confusion of the different annuli. (the distance from the centre has no meaning, they are just separated for a more accessible overview.)**
 - [x] Figure 9 is not very helpful. What do the lines outside the colored table indicate?
   * **It is a dendrogram, or as explained in the figure text: "The trees' branch length represents the Pearson distance between patients (rows) and gantry angles (columns)". I thought "tree" might be a more accessible term since that is the immediate explanation of a dendrogram on Wikipedia (https://en.wikipedia.org/wiki/Dendrogram):**

   > A dendrogram is a diagram representing a tree. This diagrammatic representation is frequently used in different contexts:
   >   * in hierarchical clustering, it illustrates the arrangement of the clusters produced by the corresponding analyses.
   >   * in computational biology, it shows the clustering of genes or samples, sometimes in the margins of heatmaps.

   * **I have changed it from "trees" to "dendrograms" and from "Pearson distance" to "relative Pearson distance" (as the absolute value cannot be extracted from the figure) in the figure text to avoid further confusion**
 - [x] Paper II: The results are so briefly mentioned that the content is almost incomprehensible.
   - Paper III: The text is again difficult to comprehend. It consists of short fragments without introduction or guidance.
   - Paper IV: This result section is not coherent for a clinician.
     * **I have, with the help of Grammarly, improved the readability and grammar of the Results chapter. It now has a Flesch readability score of 61.**

### Discussion/Conclusion
 - [x] This section is not very detailed or specific about the gain from the studies included in the present thesis.
   * **The Discussion and Conclusion have been edited to improve readability. In particular, regarding the gain from the studies, the last section of the conclusion now reads: "The methods introduced here open the doors for image- and dose-guided IMPT. The tool for scatter-correction of CBCT enables online WEPL and dose calculation. This tool also enables analyses like that used for determining robust angles. Combining these methods could enable safer proton plan delivery for sites that are subject to inter-fractional density changes."**
 - [x] Referrals to other studies show that other investigators have studied similar aspects of uncertainties due to inter fractional anatomical variations. However, the discussion does not elucidate what the unique findings are in the studies included in the present thesis.
   * **An extra paragraph describing the uniqueness of each paper has been added to the discussion**
 - [x] Again, the writing style makes the interpretation difficult.
     * **With the help of Grammarly and Hemingwayapp, the readability has been improved. It now has a Flesch readability score of 41**

## The individual papers

### Paper I:
 - Authors: Andreas G. Andersen, Oscar Casares-Magaz, Jørgen B. B. Petersen, Jakob Toftegaard, Lise Bentzen, Sara Thörnqvist & Ludvig P. Muren.
 - Title: Beam angle evaluation to improve inter-fraction motion robustness in pelvic lymph node irradiation with proton therapy.
 - Published: Acta Oncologica (2017), 56:6, 846-852, DOI: 10.1080/0284186X.2017.1317108.

 - Is the aim clear?
 - [x] The aim of this study was to investigate across a larger patient population, which beam angles were more robust to inter-fraction motion in the setting of pelvic LN irradiation for prostate cancer patients. However, this sentence does not make sense because the angles cannot be said to be more or less robust. The aim of the study must, therefore, be to explore in which beam angles the inter-fractional variations in WEPL are smallest.
   * **No. We compare across several different metrics. A more accurate statement could be: "to explore which beam angles performed better across fractions with regard to target, the organ at risk doses and the correlation to WEPL".**
 - [x] It is also stated that “We also aimed to identify any sub-populations of patients with similar patterns in the motion-robustness across beam angles, as well to explore whether robust angles could be identified using few repeat CT scans for each patient.” This sentence actually describes what the authors wanted to do, but not what was the aim of this exploration.
   * **Yes, and what we did. I do not see the difference in the "aim of this exploration".**

 - Have the methodologies been described in detail?
 - [x] The material and methods section describe the methods; however, the methods are not fully described.  The patient cohort is mentioned, but it is not clear how these 18 patients were selected for this study. The terminology about positions/location in the body are not correct for example, “the inferior end of the coccyx” and “distal surface of the LN CTV”.
   * **This was expanded in the Material & Methods section. "Distal" is in relation to the beam direction. Inferior is in the CT coordinate system, which is also known as the apex of the coccyx.**
 - [x] It is written that the CT images extended from the L4 vertebrae to the distal end of the coccyx. The scan will not include a part of the prostate if the scans extend to the caudal coccyx. The authors mention that the center of the mass of the prostate was used for rigid translational alignment and all organs at risk were contoured. If the scans extend to the caudal end of the coccyx, the whole rectum cannot be contoured. The authors do not explain how the center of the mass of the prostate is calculated, but this calculation must require that the entire prostate is included. The number of scans used for calculations and the number of calculations in each patient scans are unclear.  It appears to be a lot of calculations, but the interpretation of data is difficult without a clear impression of the amount of data.
   * **This is a drawback. At the time, I did not have sufficient knowledge of registration methods. According to the doctors involved, gold markers were used for the registration of these patients originally. We chose the centre of mass as a substitute for this. The centre of mass was calculated as usual with as much of the prostate as included in the scans. This is not always the whole prostate. The same is true for the rectum. The rectum does not necessarily have the same size on all scans. We corrected these problems in Paper-II and forward.**
 - [x] It is not clear as to the idea behind the attempt to group patients in clusters and this is not explained. Presumably, clustering requires some shared characteristics, but these characteristics are not mentioned.
   * **See the proof of concept paper (A G Andersen et al., Acta 2015, DOI:10.3109/0284186X.2015.1067720). The beauty of it is that we did not need characteristics to find these groups. The characteristics we found to distinguish the groups were mainly the magnitude of WEPL differences, as explained in the thesis and paper**
 - [x] It is stated that the average doses of the volumes are computed. The correct term must be mean dose.
   * **The average and arithmetic mean is mathematically equivalent. I used "average" to avoid confusion with terms such as geometric, weighted and harmonic mean**
 - [x] The authors write that only the PTV was included in the plan re-optimizations in order to describe the dose to the target and OAR’s with as few variables as possible.
   * **No re-optimization takes place in this study. Only re-calculation, as described.**
 - [x] The average dose was calculated for the OAR’s. This is interesting because the dose uncertainty in PT is due to variations in the volume and content of bowel, rectum and bladder. This choice needs more explanation.
   * **I did not have any better metrics at the time that would scale down to one number. We explored that further in Paper II**

 - Have the results been clearly described?
 - [x] The water equivalent path length analysis results section mentions some angles with the smallest variation and mention that WEPL maps in 11 patients have a SD > 2, mm across angles.
 - [x] The methods section did not describe what WEPL maps are.
   * **True, and some explanation should have been added. Although, it does say "Further details of the method can be found in our previous publication [13]", so no errata needed.**
 - [x] In next sentence it is stated that that the WEPL showed the least variation around the axial plane (data not shown). Does this imply that the WEPL maps in the 11 patients include different couch angles?  Therefore, the main results are different ranges of angles with the smallest WEPL variation.
   * **Yes, different couch angles were explored: "for all possible single-beam configurations,i.e. for both gantry and couch angle in 5(deg) intervals, creating a 2D map, with the left and right LNs studied separately.". Although, as couch angle = 0 seemed to have less WEPL variation for most patients, we did not perform 37(cough angles) x 37(gantry angles) x 2(left and right LNs) dose calculations, just 1 x 37 x 2 for each scan in each patient. That is what it implies. We didn't consider it the main result. Otherwise, we would have performed 37 times as many dose calculations.**
 - [x] Figure 1 and 2 include 16 patients and not 18 as stated in the materials section. A group consisting of two patients is mentioned. Per definition, 2 does not define a group.
   * **Definition of "group" from Merriam Webster: "two or more figures forming a complete unit in a composition". Although I would have preferred to call it a "set", it seemed strange to write "a set of patients".**
   * **As written in the supplementary material but mistakenly missing in the paper: "Two patients were excluded, due to outliers disturbing the visibility of colors".**
 - [x]  Figure 2 is not helpful.
   * **Figure 2 is the main result. It shows a grouping by angle-spaces (anterior, lateral and posterior) without assuming one would exist! And the two major groups of patients --- which we hoped to find.**
 - Treatment plan analysis section:
 - [x] First sentence: “For all patients throughout all scans, we found three minima of lowest dose to the bladder and bowel”. What does minima of lowest dose mean?
    * **It does seem a bit redundant - "dose minima" would have been better.**
 - [x] Second sentence: “In order of increasing dose these angles were 35, 170, and 100 for the bladder and 160, 110, and 25 for the bowel, left-right symmetrically.” Which angles? Left right symmetry? All the angles given are from the left side, so are we talking about left/right lymph nodes?
   * **Yes.**
 - [x] Next sentence “For the rectum, we found peaks of higher dose mainly at 45_, 120_ , and 180_.” What does peaks of higher dose mean?
   * **Again, just a redundancy. "dose peaks" would have been sufficient.**
 - [x] Next sentence: “Furthermore, the LNs showed most dose degradation around 145_, between 0_ and 25_ and for one patient at 65_.” It is unclear what this sentence means. Presumably, the authors mean that the dose to the lymph nodes showed largest variation (dose degradation) with a beam angle around 145_ etc.?
   * **Variation is not the same as dose degradation. We meant, as stated, dose degradation, i.e. lower dose than prescribed by the plan. Although, the variation may also have been largest at the same angles.**
 - [x] Next sentence: ”Across the DVHs for all volumes of interest, we found that the intervals 25_–35_, 100_ –110_, and 160_–170_ all showed lower OAR doses while maintaining target coverage, when compared with other angles.”. The aim was to explore which beam angles give the most robust treatment plans and here it is stated which beam angles result in the lowest predicted OAR doses.
   * **Yes, across treatments, thus more robust to interfractional changes. Remember, no re-optimization took place, only re-calculation.**
 - [x] Next sentence: “The mean-dose analysis generally showed two beam angle intervals of larger dose degradation to the bladder, prostate and rectum: 0_–20_ and 110_–130_.”. The concept of mean analysis has not been introduced in the methods section.  The term “dose degradation” is used among physicists working with proton therapy. Regardless, the term does not indicate what the magnitude of the difference is between the mentioned beam angles and other angles.
   * **The concept of "mean analysis" has not been introduced, no. Although, all we do is inspect/analyse, the "mean-dose". Hopefully, the missing explanation of that is not too crucial.**
 - [x] Next sentence: “Additionally, for the rectum direct posterior and, left-right symmetrically, 50_–60_, showed higher dose degradation.” This sentence, due to the poor writing, is difficult to understand. We have the same comment about dose degradation as above.
   * **With a slight grammar improvement: "Additionally, direct posterior and left-right symmetrically, 50_–60_, showed higher dose degradation for the rectum.". In this case, dose degradation is more dose to the OAR, i.e. rectum.**
 - [x] The bowel received an increasing amount of dose as the gantry angles approached anterior. These patterns were seen for both the 3 and 5mm margins. This must be expected due to the physical properties of protons and the location of the bowel.
   * **Exactly**
 - [x] The analysis using only two rCTs per patient identified very similar patterns, again both for the 3 and 5mm margins. Independent of margin size, the dose degradation was well described for the LN and bowel, whereas it was underestimated for the rectum and bladder. This observation brings the question about how many scans are needed in each individual into focus. There are no considerations about the number of needed scans in the paper.
   * **Not in the scope of the paper, I just pointed it out as an interesting observation for potential further studies. Of which we were planning two**
 - [x] The mean-dose cluster comparison analysis yielded the same patient groups for the rectum and bladder as for the WEPL clustering. Among the two main groups of seven and nine patients, we, respectively, observed that the posterior and anterior angles had a more favorable dose distribution (than what?). What does the term: the same patient group for rectum mean?
   * **Than other angles. "Patient groups" not "Patient group". The same grouping of patients by k-nearest-neighbour as when using WEPL instead of mean-dose to these OARs.**

 - Is the discussion exhaustive?
 - A discussion of the following aspects is missing:
 - [x] Is WEPL variation a good surrogate for plan robustness? The authors mention that they don't see a direct correlation between WEPL and dose degradation?
   * **Correct, only overall WEPL variation magnitude seemed to predict patient grouping, but not angles."**
 - [x] What is the implication of the stated main result: “By calculating, analyzing, and comparing WEPL maps and treatment planning endpoints for each side of the lymph nodes across the possible beam directions in a large cohort of patients, we have in this study developed and applied a method to identify beam angles which were more robust to organ motion for different sub-groups of patients.”?
   * **Not much, just that we applied the method from the previous paper to more patients and that we have improved/expanded it at the same time.**
 - [x] The authors write. “The angles which results in more robust plans for LN CTV are not necessarily the best for prostate coverage” The treatment for the prostate and lymph nodes is always done in one plan, so how will the findings in the present study improve the prediction of which beam angles shall be used in an individual patient?
   * **This was still being discussed at the time I left DCPT. At that time, the main option was to combine two plans due to both shortcomings of Eclipse and the maximal superior-inferior extension of the proton field.**
 - [x] What is the impact of the fact that re-optimization was done only based on PTV?
   * **No re-optimization were performed. This is very central to the study!**
 - [x] What is the relation between dose to OAR, as described in the Treatment plan analysis section, and plan robustness?
   * **It depends on what you mean by "dose to OAR". Suppose you mean the average dose throughout all rCTs, the dose at each rCT, or the dose on the pCT. For anything but the dose in the pCT, the answer is self-defining. If you mean the dose to the pCT, we did not investigate that correlation, as it was not in the scope of the paper, but you may be able to read it from the mean-dose angular charts.**
 - [x] What are the implications of patient grouping based on which beam angles result in the most robust treatment plans?
   * **Not much, as it turns out in one of the papers of Kia Busch. The posterior angles seemed to be sufficiently robust for all patients. The patient grouping was not needed.**
 - Aim/conclusion “Does the study answer the asked questions?
 - [x] Aim: The aim of this study was, therefore, to investigate across a larger patient population, which beam angles were more robust to inter-fraction motion in the setting of pelvic LN irradiation for prostate cancer patients. We also aimed to identify any sub-populations of patients with similar patterns in the motion-robustness across beam angles, as well as to explore whether the robust angles could be identified using few repeat CT scans for each patient.” Conclusion: “In conclusion, we have in this study found that differences in WEPL maps and mean dose to OARs show population-specific patterns and that there were consistent patterns in which angles were most robust. Similar “robust” angles were also found in the DVH analysis.” It is unclear what the term “population-specific patterns” means and we are not sure what “consistent patterns in which angles were more robust” means? The second aim, to identify subpopulations with similar patterns, is not mentioned in the conclusion, even though the topic is explored in the study.
   * **The phrase "population-specific patterns" was written by Ludvig. I understand it as patterns specific to patient types. I.e. groups of patients that share a common (but unknown) trait. Thus, a trait that would cause a different pattern from another group. The phrase "consistent patterns in which angles were more robust" means that the patterns in mean-dose vs angle we saw were consistent throughout rCTs and across patients within the "subpopulations", i.e. the groups we found in the k-nearest-neighbour analysis.**

 - Is the paper satisfactory overall?
 - [x] This study is published in a peer-review journal. However, from a clinical standpoint the paper is very difficult to read and the technical methods and results are certainly not explained in a way which makes the data accessible for clinicians.
   * **It was not meant for clinicians but other researchers. The plan is simplified on purpose. Not to reflect a clinical plan but to reduce variables.**

### Paper II:
 - Authors: Kia Busch, Andreas G. Andersen, Oscar Casares-Magaz, Jørgen B. B. Petersen, Lise Bentzen, Sara Thörnqvist & Ludvig P. Muren.
 - Title: Evaluating the influence of organ motion during photon vs. proton therapy for locally advanced prostate cancer using biological models.
 - Published: Acta Oncologica (2017), 56:6, 839-845, DOI: 10.1080/0284186X.2017.1317107.

 - Is the aim clear?
 - [x] The aim as written in the paper: "The aim of this study was therefore to compare PT with contemporary photon-based RT and in particular evaluate the influence of inter-fractional organ motion for locally advanced prostate cancer, including the use of biological models". The aim does not state what is actually compared and the type of biological model is not mentioned.
   * **PT and RT are compared. Specifics are in Materials & Methods, as they should be**

 - Have the methodologies been described in detail?
 - [x] The methodologies are overall described well.
 - [x] In the "Patient cohort and online volumetric imaging" section there is a lack of information about how the sets of scans were selected, from the larger cohort from paper I (basically the authors included sets of scans and not patients). It was a surprise that no bladder/rectum filling protocols were applied, but this probably does not affect the study. The scans are quite old, from before 2008, but the needed information can be gathered anyway. It is unclear if the planning CTs are matched with MRI for contouring, which is the standard for modern prostate radiotherapy.
   * **The contouring is better described in previous papers as cited: "Further information on the definition of OARs and CTVs can be found in Thörnqvist et al. [13]."**
 - The "Treatment Planning" section:
 - [x] Perhaps the method for matching the scans could be mentioned, for instance, did the authors write that they did rigid alignment using the isocenter of the prostate? The figures show fiducial markers in the prostate, so presumably they were used for matching as the prostate is not clearly visible on a CT scan.
   * **We meant the "isocentre of the prostate". We did not have the skills to use any sophisticated registration software at the time.**
 - [x] The "Data analysis section": The gEUD calculation method is not described and a value 12 and 8 is mentioned for the parameter α, which is not described and the term α was not used in reference 15, to which the authors refer. Why is the Lyman–Kutcher–Burman model chosen as the NTCP model; reference 16 may not give the answer. Both reference 15 and 16 are based on outdated radiotherapy. The doses to femoral heads should be shown as opposing lateral beams are used for one of the proton plans in each set of scans.
   * **I hope these references were not outdated at the time since they and the LKB model were advised/approved by our supervisors. We have learned more about biological models since. gPMC were investigated with the original purpose of fast MC-based LET calculation for creating new LET-based biological models. Jesper Pedersen was supposed to perform that study but had complications getting beamline models for the MC simulation.**

 - Have the results been clearly described?
 - [x] The results clearly described.

 - Is the discussion exhaustive?
 - The discussion could have covered some aspects in more details:
 - [x] Why is the Lyman–Kutcher–Burman model chosen. Is this model the best tested model?
   * **As mentioned earlier, it was advised by our supervisors. We did not know better models until a later study by Pedersen et al., RadOnc, DOI: 10.1016/j.radonc.2020.10.025**
 - [x] The issue described on page 844 about PT sparing the organs at risk in the lower dose range, but not in the high dose range is interesting and important. How does the Lyman–Kutcher–Burman model take this difference into account?
   * **gEUD weighs the dose-response according to 'a' and alpha/beta values based on RT data. This is the only way we account for it. There is a good description of how it impacts gEUD here: https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5768006/#acm212224-sec-0002title**
 - [x] The choice of toxicity used for the NTCP comparisons between PT and VMAT: Is this the most relevant toxicity? Could other toxicities show benefit of PT or VMAT? Why is the no difference in NTCP, when mean doses and V98 for organs at risk are lower with PT.
   * **"Lyman–Kutcher–Burman NTCP parameters for rectum (grade 2 rectal bleeding) [16] and bladder (obstruction) [17].". These two endpoints were chosen because the data (in table 1) were available for them. Moreover, these also seem to be among the most common metrics investigated for prostate cancer treatment. About the difference, see the point on gEUD above.**
 - [x] Reference 20 is an important reference in the discussion section. This paper states: "The NTCP estimates are population-based: a low risk estimate does not preclude the occurrence of rectal injury, possibly severe, in any individual patient". This is important information because the idea behind the present study is to relate NTCP to the choice of treatment. The reference 20 paper states that "It is possible that intensity-modulated radiotherapy or proton beam dose distributions require modification of these models because of the inherent differences in low and intermediate dose distributions".  As reference 20 is a central paper for the discussion sections and no later papers about NTCP in prostate radiotherapy are cited in this discussion, these aspects could have been addressed in more details. The authors of the present paper only mention very briefly on page 844 "There might be low dose dependent toxicity, which is not accounted for in the chosen model".
   * **I'm not sure what more needs to be said. Our group explored the applicability of biological models to PT later: Pedersen et al., RadOnc, DOI: 10.1016/j.radonc.2020.10.025**
 - Specific comments to the discussion section:
 - [x] The sentence on page 844 states: "In a previous study from our group, Andersen et al. [9] found the lateral angles to be less robust due to density changes, while two lateral oblique beams were more robust". Presumably the authors mean that the variation of density (WEPL) is larger if lateral beams are used while less variation in density was seen when two lateral oblique beams were used.
   * **We meant less robust - mainly in terms of dose. WEPL variation was not used as a metric for robustness in that paper but rather as a predictor of patterns. Maybe this is continued confusion about re-optimization vs re-calculation.**
 - [x] Next sentence: "It should be pointed out that in this previous study, each lateral section of the LNs was targeted separately without irradiating the prostate (with single beam plans), and also a different TPS was used". This sentence is confusing as no patients were treated according to the plans in the mentioned study.
   * **Do you mean irradiating is a confusing term when no actual but only simulated treatment is performed? It could probably have been phrased better: "[...] without including the prostate in the plan [...]".**
 - [x] Next sentence: "Two lateral opposed beams may not be suitable if there are very large variations in the rectum or bladder during the treatment course. This was studied by Thörnqvist et al., where an additional margin (up to 10 mm) was not sufficient to fully account for the dose degradation in the seminal vesicles and LNs." It is not clear that dose degradation can happen in the seminal vesicles and LN's and not understandable how an additional margin would prevent dose degradation.
   * **Margins are used mainly to avoid dose degradation to the target. With dose degradation to a target, we mean lower than prescribed dose.**
 - [x] Next sentence: However, they did find the prostate target to be robust when fiducial marker-based positioning was used. Not sure what a robust prostate target is?
   * **This should probably be "prostate target coverage".**
 - [x] Next sentence: "In our study, we found poor target coverage of the LN CTV for V98%, however the ratio between the delivered and planned dose were close to unity. We also found the prostate target to be robust when using VMAT and two lateral opposed beams. The latter have the disadvantage that the dose to the femoral heads increases, when compared with photon-based RT. However, Valery et al. recently reported no increase in the risk of hip fracture nor hip pain when treating prostate cancer patients with PT." The doses to the femoral heads could have been shown.
   * **Yes. The femoral heads were only delineated in a few scans. We were considering doing auto-segmentation of bone to get that data as well.**
 - Conclusion
 - [x] Page 845: "In conclusion, two lateral opposed beams can be used when treating locally advanced prostate cancer with PT. This field configuration gave the best dose coverage of the prostate CTV and covered the LN CTV sufficiently based on the mean dose.". We are surprised that the authors based on this study makes this conclusion without any reservations.

 - Is the paper satisfactory overall?
 - [x] This is a paper based on 9 scans from 8 patients, investigating the impact of variation in anatomy during the course of radiotherapy on the estimated doses of prostate and pelvic lymph node proton therapy, compared to photon radiotherapy. This rather small study obtained knowledge, which is interesting, however the study does not create high level evidence. The paper has some issues about the methodology and the discussion is not at a satisfactory academic level.
   * **I am open to an errata if you think it is necessary. But the suggestions above do not warrant one in my opinion.**

### Paper III:
 - Authors: Andreas G. Andersen, Yang-Kyun Park, Ulrik V. Elstrøm, Jørgen B. B. Petersen, Gregory C. Sharp, Brian Winey, Lei Dong & Ludvig P. Muren.
 - Title: Evaluation of an a priori scatter correction algorithm for cone-beam CT projections in photon vs. proton therapy gantries.
 - Published: Physics and Imaging in Radiation Oncology (2020), 16, 89-94, DOI: 10.1016/j.phro.2020.09.014.

 - Is the aim clear?
 - [x] This is a collaborative study between different major institutions active in the field of proton therapy and CBCT-image guidance, aiming to evaluate the performance of a previously proposed scatter correction algorithm for CBCT on two different clinical systems installed at photon and proton gantries. The aim is clearly given.

 - Have the methodologies been described in detail?
 - [x] The methodologies are overall described well. Most of the original parts of the work seems to be related to the readout of proprietary data formats and efficient implementation of the original algorithm, while it might have been beneficial to learn if further tweaks were needed due to the different imaging characteristics (e.g., different source-detector distances, use or not of Bowtie filters). The choice of the DABEST analysis could have been better justified.
   * **Further tweaks were needed but of a specific and technical nature. I.e. choice of coordinate systems and file formats by the vendors. Bow-tie filters had to be accounted for as well in different ways for each Varian file format. The primary issue was the inconsistent projection brightness between scans, with no normalization parameter in the files and no correlation to voltage or current.**

 - Have the results been clearly described?
 - [x] The results clearly described.

 - Is the discussion exhaustive?
 - [x] The discussion is quite comprehensive and nicely describes works of other groups around this topic. Given that the purpose was to compare the performance of CBCT scatter correction on systems mounted on photon and proton gantries, a more detailed investigation of the different behavior would have been beneficial, beyond the only given considerations that “The proton gantry CB had a longer SID and no bow-tie filter, which could be part of the reason for the differences. However, the proton gantry CB used a different pulse current than the photon gantry CB, so we avoid a direct comparison, although both reflect the clinical standard for the given gantry”. Also, a comparison of the imaging dose at the target would have been beneficial to compare the results.
   * **Additional differences are primarily proprietary and very hard to reverse-engineer (if legal). As stated, we did not want to compare them directly and therefore avoided the imaging-dose discussion.**

 - Is the paper satisfactory overall?
 - [x] Overall, the paper is satisfactory. Given the fact that it was the first comparison of the same (and largely acknowledged as most promising) CBCT scatter correction approach on different systems at photon and proton gantries, the restriction to phantoms of well-known ground truth is certainly well justified. Limitations of the study are mostly well acknowledged, and although a more direct explanation of the observed differences would have been beneficial, the paper remains an interesting piece of work in the field.
   * **Thanks!**

### Paper IV:
 - Authors: Kia Busch§, Andreas G. Andersen§, Jørgen B. B. Petersen, Stine E. Petersen, Heidi S. Rønde, Lise Bentzen, Sara Pilskog, Peter Skyt, Ole Nørrevang & Ludvig P. Muren. (§ Joint authorship).
 - Title: Towards range-guided proton therapy to detect organ motion induced dose degradations.
 - Submitted: Physics in Medicine & Biology — IOP Science.

 - Is the aim clear?
 - [x] The aim of the paper is to investigate the potential of range-guidance using water equivalent path length (WEPL) calculations to detect dose degradations occurring in PT. As explained more in detail in the discussion, this goal is tightly linked to the work of Paper III, as this could be a tool to be deployed on scatter corrected CBCT to provide a certain level of alert prior to treatment delivery, rather than requiring a more complex dose recalculation.

 - Have the methodologies been described in detail?
 - [x] The methodologies are overall described well, although some more considerations on the selected patient cohort and treatment planning approach could have been beneficial (this aspect is especially touched upon toward the end of the manuscript in relation to a planned large scale clinical study in Denmark, but such considerations could have been anticipated). Also, although the authors stress the advantage of avoiding the need of re-delineation, it would be useful to include in the analysis also the distance between the WEPL and the distal target contour (given the fact that this information seems to be available). This would enable to better compare their new introduced metrics to previously proposed approaches (especially considering that many solutions are upcoming to solve the issue of auto-contouring).
   * **Interesting idea. We attempted something similar but, if I recall correctly, we concluded the correlation would not link causation properly by the nature of the distance metrics.**

 - Have the results been clearly described?
 - [x] The results clearly described. As mentioned, it would have been nice to also include an analysis more along the lines of the approach proposed by Kim et al, since the authors should have the recontoured structures in the rCT and could have added this comparison to better support the advantage of their method (besides the mentioned avoidance of re-delineation, which could be anyhow overcome in the future by the active research in this field using deep learning methods).
   * **See the response above. In addition, I think one could use target delineations instead of iso-doses. Still, the correlation to the influence on the plan may be weaker.**

 - Is the discussion exhaustive?
 - [x] The discussion is quite comprehensive and nicely addresses the limitations of the presented study, as well as the main approaches / findings of other groups. As mentioned, a more direct comparison with the method of Kim et al, would have been appropriate and some more considerations on the prospects of auto-contouring, which is at the moment a hot topic of research, for which first commercial solutions are emerging at least in the context of photon therapy.
   * **A paragraph has been added to Paper IV**

 - Is the paper satisfactory overall?
 - [x] Although it would be quite interesting to know the status of the paper after submission, it can be for now said that the paper has merits for example in introducing new ways of using the WEPL calculation as an alert prior to treatment delivery. However, as acknowledged by the authors, the results are likely strongly dependent on the considered anatomical indication and used planning technique, so that many more investigations will be needed in order to make this method a valuable tool for QA in proton therapy. Moreover, a direct comparison with other approaches from the literature (e.g., from Kim et al), as well as a more detailed discussion of new horizons on auto-contouring would have been beneficial (considering that one of the main advantages of the proposed work is claimed to be the avoidance of the need of target re-delineation).

## Conclusion

 - [x] The chapter that reviews the entire thesis is generally poor and needs extensive revision.
   * **Done.**
 - [x] The thesis itself is based on four papers. Although three of these have been published in peer-reviewed journals with reasonable impact factors, there were numerous issues that were not adequately addressed. While these cannot be changed in the three published papers, they could be addressed in the review chapter.
   * **The specific issues above have now been addressed.**
 - [x] Paper IV has not yet been published and if it has not yet gone through peer-review then we strongly encourage the Ph.D. student to expand the discussion on the needs for delineation (on the basis of recent activities in the field of deep learning for auto-segmentation), since this could also be an additional information to be added to the WEPL-based range guidance, to assess if the tumor after possible changes could still be covered by the beam.
   * **A paragraph was added to the discussion. A similar paragraph was also added to Paper IV.**
 - [x] Moreover, a more direct comparison to previously proposed approaches would have been beneficial to better highlight the merits of the new work. There are also concerns about papers II and IV being used in a previous thesis. The candidate has supplied additional documentation clearly describing what work he did on these two papers, but we were unable to assess whether the data from papers II and IV were used differently in the two theses.
   * **The comments above should clarify some of the differences.**

```
Overall, the assessment committee does not find the current presentation of the Ph.D. thesis to be of sufficient quality in line with the international standard for PhD dissertations in the field of health sciences. However, we are willing to allow the Ph.D. student to resubmit an extensively revised version. Thus, the assessment committee recommends that the dissertation should be returned for revision and the Ph.D. student be allowed three months to resubmit that revised version.




___________________________________________________
Michael R. Horsman



___________________________________________________
Katia Parodi



___________________________________________________
Peter Meidahl Petersen

```


"not very accurate" is used a few times in the opponents' comments. If it is genuinely inaccurate, please be more precise on how so. I have answered as if they meant "not very detailed" or "not very precise".

Additionally to the changes referenced above, three paragraphs have been added to the introduction, citing other works on scatter correction, WEPL, and robustness.
