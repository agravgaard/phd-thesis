\documentclass[english,a4paper,oneside,onecolumn,article,12pt]{memoir}
% \pdfminorversion=7
\pdfvariable minorversion=7
\usepackage[english]{babel}
\usepackage[utf8x]{inputenc}
\usepackage{graphicx} % for images
\usepackage[space]{grffile}
\usepackage{mathtools} % Extra maths... Let it be, you might need it.
\usepackage{color}
\usepackage{array}
\usepackage{pdfpages}
\usepackage{epstopdf}
\usepackage{float}
\usepackage{cite}
\usepackage{caption}
\usepackage{acro} % For abbreviations page
\usepackage{enumitem} % For description based lists (i.e. Paper I ... Paper V)
\sloppy
\definecolor{lightgray}{gray}{0.5}
\setlength{\parindent}{0pt}
\linespread{1.3}
\usepackage{url}
\usepackage[colorlinks=false]{hyperref}
\usepackage[draft]{fixme} % fixme notes \fxnote{} for your self

\usepackage{nameref} % For referencing sections by name instead of number
\usepackage{listings} % For including code samples.

\renewcommand{\tref}[1]{\tablename~\ref{#1}}
\renewcommand{\fref}[1]{\figurename~\ref{#1}}
\newcommand{\lref}[1]{Equation~\eqref{#1}}
\newcommand{\matl}[0]{\textsc{Matlab}} % Matlab with small caps.
% Roman numerals \RNum{2021}:
\newcommand{\RNum}[1]{\uppercase\expandafter{\romannumeral #1\relax}}

% Add acronym:
\newcommand{\addac}[2]{
  \DeclareAcronym{#1}{
    short = #1 ,
    long  = #2 ,
    tag = abbrev
  }
}
\newcommand{\tyvept}	{\fontsize{20pt}{22pt} \selectfont}

\renewcommand\Re{\operatorname{Re}}
\renewcommand\Im{\operatorname{Im}}

% Caption styles: Makes all captions sans serif and makes them slightly smaller.
\captionnamefont{\small\small\sffamily}
\captiontitlefont{\small\small\sffamily} 


\usepackage{soul} % For front page stuff
% \sodef\an{}{0.2em}{.9em plus.6em}{1em plus.1em minus.1em}
% \newcommand\stext[1]{\an{\scshape#1}}
% \input{macro}

\lstdefinestyle{customc}{
  belowcaptionskip=1\baselineskip,
  breaklines=true,
  frame=L,
  xleftmargin=\parindent,
  language=C,
  showstringspaces=false,
  basicstyle=\footnotesize\ttfamily,
  keywordstyle=\bfseries\color{green!40!black},
  commentstyle=\itshape\color{purple!40!black},
  identifierstyle=\color{blue},
  stringstyle=\color{orange},
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage{tikz}
\usepackage[compat=1.1.0]{tikz-feynman}
\usetikzlibrary{calc,trees,positioning,arrows,chains,shapes.geometric,%
	decorations.pathreplacing,decorations.pathmorphing,shapes,%
	matrix,shapes.symbols}

\tikzset{
  invisible/.style={opacity=0},
  visible on/.style={alt={#1{}{invisible}}},
  alt/.code args={<#1>#2#3}{%
    \alt<#1>{\pgfkeysalso{#2}}{\pgfkeysalso{#3}} % \pgfkeysalso doesn't change the path
  },
}

\pgfdeclarelayer{back}
\pgfsetlayers{back,main}

\usepackage{textcomp}
\usetikzlibrary{shapes,arrows}
% Definition of blocks:
\tikzset{%
  block/.style    = {draw, thick, rectangle, minimum height = 3em,
    minimum width = 3em, inner sep = .3cm},
  sum/.style      = {draw, circle, node distance = 2cm},
  ellip/.style    = {draw, ellipse, node distance = 2cm, minimum height = 2.5em},
  input/.style    = {coordinate}, % Input
  output/.style   = {coordinate} % Output
}


\tikzset{
	>=stealth',
	punktchain/.style={
	rectangle,
	rounded corners,
	% fill=black!10,
	draw=black, very thick,
	text width=10em,
	minimum height=3em,
	text centered,
	on chain},
	line/.style={draw, thick, <-},
	element/.style={
	tape,
	top color=white,
	bottom color=blue!50!black!60!,
	minimum width=8em,
	draw=blue!40!black!90, very thick,
	text width=10em,
	minimum height=3.5em,
	text centered,
	on chain},
	every join/.style={->, thick,shorten >=1pt},
	decoration={brace},
	tuborg/.style={decorate},
	tubnode/.style={midway, right=2pt},
}

% Defining string as labels of certain blocks.
\newcommand{\suma}{\Large$+$}
\newcommand{\subt}{\Large$-$}
\newcommand{\fdk}{\textbf{FDK}}
\newcommand{\smooth}{$f(CF \times I_{raw} - I_{pri})$}
\newcommand{\inte}{$\displaystyle \int$}
\newcommand{\derv}{\huge$\frac{d}{dt}$}


%%%%%%%%% For angular chart %%%%%%%%%%
\usepackage{xcolor}
\usepackage{xparse}


\usetikzlibrary{calc,positioning}
\usetikzlibrary{decorations, decorations.text}

\newlength{\layerwd}
\newcounter{outermost}

\NewDocumentEnvironment{onion}{sm}{%*= draw axes; #1: thickness of each annulus
    \begin{tikzpicture}
        \setlength{\layerwd}{#2}%
        \setcounter{outermost}{0}
        \IfBooleanT{#1}{%
            \draw[black, thick] (0,3.5) -- (0,3) node[pos=-.5]{$0^{\circ}$};
            \draw[->] (0,3.4) arc (90:45:3.4);
            \draw[black, thick] (-3.5,0) -- (-3,0) node[pos=-.9]{$270^{\circ}$};
            \draw[black, thick] (0,-3.5) -- (0,-3) node[pos=-.5]{$180^{\circ}$};
            \draw[black, thick] (3.5,0) -- (3,0) node[pos=-.9]{$90^{\circ}$};
            % \draw[<->] (0,4) -- (0,-4);
        }
}{%
    \foreach \A in {0,...,\theoutermost}{\draw[thick] (0,0) circle (\A*\layerwd+\layerwd);}
    \end{tikzpicture}
}

% *=text on a circular path; [optional fill color]; #3 layer;
% #4 start angle; #5 stop angle; #6 [optional text]
%% angles are counterclockwise in degrees
\NewDocumentCommand{\annulus}{sO{lightgray}mmmo}{%
    \filldraw[thick,fill=#2] (#4:#3*\layerwd) %% start here
        arc [radius=#3*\layerwd, start angle=#4, delta angle=#5-#4] %% inner arc
        -- (#5:#3*\layerwd+\layerwd) %% move out
        arc [radius=#3*\layerwd+\layerwd, start angle=#5, delta angle=#4-#5] %% outer arc
        -- cycle; %% Back to the beginnning
    \pgfmathsetmacro{\tmp}{(#5-#4)/2 +#4} %% Locate the middle of the arc
    \IfNoValueF{#6}{%
        \IfBooleanTF{#1}
        {%
            \begingroup
                \def\\{\space} %% A safety precaution, \\ = space on decorated text
                \path[rotate=\tmp-180,postaction={
                    decorate,
                    decoration={
                        text along path,
                        raise=-3pt,
                        text align={align=center},
                        reverse path=true,
                        text=#6
                    }
                }] (0,0) circle (#3*\layerwd+0.5*\layerwd);
            \endgroup
        }%%
        {%
            \node[inner sep=0pt, %%% If there is text, print it
            text width=#3*\layerwd*3+\layerwd,
            align=center,
            rotate=\tmp-90,
            font=\footnotesize] at (\tmp:#3*\layerwd+0.5*\layerwd)
            {#6};
        }%
    }%
    \ifnum\theoutermost<#3\setcounter{outermost}{#3}\fi
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5

\usepackage{microtype} % Attempts to fix badbox problems. Only compatible with pdflatex.

%                     left  right ratio
\setlrmarginsandblock{20mm}{20mm}{*} % asterisk means fill out automatically
%                     top   bot   ratio
\setulmarginsandblock{20mm}{20mm}{*}
%\setlength{\oddsidemargin}{0cm} % Gives more room on the page

\checkandfixthelayout
\pagestyle{simple} %Gives empty footer and page number in header


\input{tex/abbreviations}

\begin{document}

\input{tex/contents}

\end{document}

