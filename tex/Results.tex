\chapter{Results}

\section{Paper I}

Eleven out of the 18 patients investigated in this study showed more
considerable variations in \ac{WEPL} (SD $ > 2.5$ mm), peaking at the
lateral angles $[80^{\circ}, 100^{\circ}]$. In contrast, the remaining
seven patients did not have any clear pattern in the \ac{WEPL} maps.
The \ac{WEPL} variation for the left and right \ac{LN}s
showed a general pattern of being lower around a couch angle of
$0^{\circ}$, i.e.\ the axial plane. For this reason, only the axial
plane was further
investigated (\nameref{PaperRobust}).\medskip

The clustering analysis of \ac{WEPL} maps against gantry angles showed
that the patients split into three groups for the left section of the
\ac{LN}s.  One of these groups consisted of just two patients who
showed a distinct lower variation pattern in the lateral and posterior
angles.  These patients instead showed the highest variation in the
lateral angles. The \ac{WEPL} clustering for the right section of the
\ac{LN}s split into two groups of seven and nine patients, which
differed by having higher \ac{WEPL}-variation in the posterior and lateral
angles (\nameref{PaperRobust}).\medskip

\input{tex/tikzfigs/anglechart}

The bladder and bowel had three general left-right symmetrical minima
regarding dose throughout all patients' scans. The rectum instead
showed peaks of high dose. The \ac{LN}s showed the most considerable
dose degradation around $145^{\circ}$, in $[0^{\circ}, 25^{\circ}]$
and for one patient at
$65^{\circ}$ (\nameref{PaperRobust}).
\fref{fig:anglechart} illustrates a summarization of all the most
significant angle intervals.\medskip

The same patterns could be identified with only two \ac{rCT}s per
patient, both for the $3$ and $5$ mm margins. The magnitude of dose
degradation was captured well for the \ac{LN} and bowel while
underestimated for the rectum and
bladder (\nameref{PaperRobust}).\medskip

The mean dose clustering analysis yielded the same patient groups for
the rectum and bladder when using \ac{WEPL}. The two major groups of
seven and nine patients favoured the posterior and anterior angles,
respectively
(\fref{fig:cluster_dose}) (\nameref{PaperRobust}).

\begin{figure}[ht]
    \includegraphics[width=\textwidth]{img/cluster_dose.jpeg}
    \caption{Cluster comparison for the $\log_2$ normalized mean dose
    to the bladder. A higher "Value" (top left) corresponds to a
    higher mean dose variation. The dendrograms' branch length
    represents the relative Pearson distance between patients (rows)
    and gantry angles (columns). For the right section of the LNs with
    a $5$ mm isotropic margin. (One patient was excluded due to
    outliers disturbing the visibility)
     (\nameref{PaperRobust}).} \label{fig:cluster_dose}
\end{figure}

\section{Paper II}

The proton plans achieved a reduced dose to the rectum and bladder,
compared to the \ac{VMAT} plans, although with a more considerable
variation across the \ac{rCT}s. The \ac{gEUD} for these \ac{OAR}s were
similar across all three plans (\nameref{PaperNTCP}).\medskip

The rectum and bladder \ac{NTCP} values ranged from $1$ to $4\%$ for
the \ac{VMAT} plans and $0$ to $14\%$ for the proton plans.  The
median \ac{NTCP} for both the \ac{VMAT} and proton plans were $1-2\%$
for the bladder and $15-17\%$ for the
rectum (\nameref{PaperNTCP}). The \ac{NTCP} values correlated
linearly with the \ac{PTV}'s overlap with the respective \ac{OAR} in
the \ac{pCT} \fref{fig:KiaNTCP}.\medskip

The mean and $V_{98\%}$ doses were similar for the \ac{VMAT} and
proton plans. The proton plan with lateral fields showed the lowest
variation in mean dose ($74-75$ Gy) and $V_{98\%}$ ($80-99\%$).
Meanwhile, the proton plan with anterior oblique fields had the
highest mean dose variation ($71-76$ Gy). This plan and the \ac{VMAT}
plan both showed a high variation in $V_{98\%}$
($32-100\%$) (\nameref{PaperNTCP}).\medskip

All plans covered the \ac{LN} \ac{CTV} target sufficiently, with
\ac{VMAT} achieving the highest median $V_{98\%}$ of $98.5\%$. For
lateral and anterior oblique fields, the proton plans had a lower
median $V_{98\%}$ of $76.7\%$ and $72.4\%$,
respectively (\nameref{PaperNTCP}).

\input{tex/figures/KiaNTCP}

\section{Paper III}

The CT number segmentation analysis compared the \ac{rigidCT} against
the \ac{corrCBCT} and \ac{stdCBCT} for the proton and photon
gantry CB and the \ac{iterCBCT} for the photon gantry.  The
\ac{corrCBCT} was generally more similar to the \ac{rigidCT}. The
\ac{corrCBCT} of the photon gantry performed the worst for low-density
air segmentations, although still within
\ac{SD} (\nameref{PaperCBCT}).\medskip

The most significant differences for this analysis were seen in the
$50\%$ bone segmentation. The median CT numbers of the \ac{stdCBCT}
and \ac{iterCBCT} were around $300$ HU higher than the \ac{rigidCT} in
this segmentation. The \ac{corrCBCT} were $72$ and $57$ HU higher by
median than the \ac{rigidCT} for the proton and photon gantry \ac{CB},
respectively (\nameref{PaperCBCT}).\medskip

The Alderson phantom \ac{WEPL} calculations also showed better
agreement between the \ac{rigidCT} and the \ac{corrCBCT} than with the
\ac{stdCBCT} and \ac{iterCBCT}, both in median and \ac{SD}. The median
\ac{WEPL} differences per sub-region ranged from $-6.0$ to $7.4$ mm
for the \ac{corrCBCT}. The median differences in the \ac{stdCBCT} and
\ac{iterCBCT} ranged from $2.3$ to $57.5$
mm (\nameref{PaperCBCT}).\medskip

The \ac{corrCBCT} had a higher gamma pass rate for all but one plan by
comparison of dose distributions. Moreover, the \ac{stdCBCT} and
\ac{iterCBCT} had an equivalent performance for this
analysis (\nameref{PaperCBCT}). The mean paired differences
between the \ac{corrCBCT} and \ac{stdCBCT} was $19$ and $7$ percentage
points, respectively, for the proton and photon gantry \ac{CB} with a
tolerance threshold of $2\%/2$ mm \fref{fig:gamma}.

\input{tex/figures/dabest_gamma}

\section{Paper IV}

The spatial agreement between the iso-WEPLs and isodoses was
scored as the percentage of points with a Hausdorff distance lower
than a given threshold. The mean spatial agreement was $88\%$ for a
$2$ mm threshold across all patients and \ac{rCT}s for the
$90^{\circ}$ field's $25\%$ iso-level. The agreement increased with
the threshold to $96\%$ for $3$ mm, and $99\%$ for both $4$ and $5$
mm.\medskip

The left posterior oblique $150^{\circ}$ field had the lowest mean
spatial agreement of $65\%$ for a $2$ mm threshold at the $25\%$
iso-level. Likewise, the mean spatial agreement of this field and
iso-level increased with the threshold to $83\%$ for $3$ mm, $94\%$
for $4$ mm, and $94\%$ for $5$ mm.\medskip

Dose degradation in the prostate CTV could be detected through range
variations measured as the spatial agreement between the iso-WEPLs and
the \ac{pCT} isodose. The values for sensitivity, specificity,
\ac{PPV} and \ac{NPV} were similar across iso-levels. The field and
Hausdorff threshold had the most significant impact on these values.
E.g.\ the $270^{\circ}$ field had a sensitivity of $73\%$ for an
iso-level of $25\%$. However, increasing the Hausdorff threshold to
$5$ mm reduced the sensitivity to $18\%$
\fref{fig:wepl_detect}.\medskip

The \ac{LN} and \ac{SV} CTV had sufficient dose coverage in most
\ac{rCT}s. This sufficiency also meant results of almost only true
positives and false negatives.

\input{tex/figures/wepl_detective}
