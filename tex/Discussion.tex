\chapter{Discussion}

This thesis presents and investigates a collection of different approaches to
account for inter-fractional motion in proton therapy.  The first approach was
to select beam angles more robust towards anatomical changes that influence the
dose distribution.\medskip

\nameref{PaperRobust} investigated a method\cite{Andersen2015c} for obtaining
these angles and found in particular two configurations: Anterior oblique
fields, which were explored further in \nameref{PaperNTCP}, and posterior
fields in \nameref{PaperWEPL}.  \nameref{PaperNTCP} compared more sophisticated
plans with a focus on NTCP and target coverage.  The plans used multi-field
set-ups with optimization for prostate target coverage and sparing of the
\ac{OAR}s. However, we found no benefit from the anterior angles. Although, the
proton plans did perform better than the \ac{VMAT} plan.  \nameref{PaperCBCT}
addressed using these quantitative methods with images taken at the treatment
gantry, i.e.\ \ac{CBCT}s. It optimized an existing algorithm for scatter
correction. The improvements from the corrections were evaluated using range
and dose calculations.  \nameref{PaperWEPL} introduced a novel method for
swiftly estimating dose deterioration on repeat scans. It found that the
iso-WEPLs were able to capture dose deterioration.\medskip

The scatter correction application developed for \nameref{PaperCBCT} is a
powerful tool that has been extended in several ways. New features include, but
are not limited to:
\begin{itemize}
  \item WEPL calculation
  \item Transferring and registering structures both rigidly and deformably
  \item The GPU-accelerated Monte Carlo dose calculation support
\end{itemize}

The application has been optimized and automated so that it can be used in a
clinical setting. Unfortunately, the CB scanner and proton-gantry vendor have
not yet attached a frame-grabber. The integration has thus been on hold for
more than a year. However, in the meantime, two projects have done the
scaffolding work. The first investigated capturing frames and sending them to
the application in a structured queue\cite{pilgaard_optimizing_2019}. The
second simplified the UI for a straightforward workflow providing an interface
similar to the clinical
software\cite{norbygaard_brugergraenseflade_2019}.\medskip

The WEPL tool from \nameref{PaperWEPL} is meant for flagging changes for
further investigation. It was developed to be visualizable and fast. In a
clinical setting, there is no time to lose. The \ac{CBCT} is a snapshot in
time; any time spent between that and the dose delivery, the anatomy may change
again. The iso-WEPL calculation employs pre-calculated isodoses from the
\ac{pCT}. It does not need anything other than a repeat scan to calculate the
iso-WEPL in under a second.\medskip

Moreover, the scatter correction method from \nameref{PaperCBCT} would enable
using the \ac{corrCBCT} as the repeat scan. It will already be registered
to the \ac{pCT} and the structures with it --- including the isodoses.
Furthermore, when combining this method with a frame-grabber, the lead time
from \ac{CB} capture to iso-WEPL should be less than 50 seconds.\medskip

This time has further potential for reduction. A project called ScatterNet by
Hansen et al.\cite{hansen_scatternet:_2018} showed how the scatter correction
method's output could train a convolutional neural net. The neural net learned
to generate the scatter maps from the CB projections. This network would cut
several time-consuming steps in the scatter correction process.  The deformable
registration would be unnecessary. The back- and forward-projections would be
reduced into one back-projection after an inference step.\medskip

The bladder is sensitive to "hotspots" associated with late genitourinary
toxicity\cite{Cheung2007}. The anterior oblique angles could overlap with the
bladder for some patients, resulting in such hotspots.  Therefore, these angles
and the \ac{NTCP} of the bladder were intriguing subjects for further study in
\nameref{PaperNTCP}.  Furthermore, chronic rectal bleeding and loss of bowel
function are common late side effects\cite{Michalski2010}. For this reason, the
posterior angles may be recommended for being most robust, with the best
sparing for the bowel and rectum. For the same reason, these angles were
further explored in \nameref{PaperWEPL}.  \medskip

\nameref{PaperRobust} only considered the \ac{LN}s, so angles found to be more
robust does not necessarily apply to the prostate\cite{Cao2015}. Consequently,
further investigation was performed in \nameref{PaperNTCP} and
\nameref{PaperWEPL}. For \nameref{PaperWEPL} and in preparation for an upcoming
clinical trial, several different beam angle combinations were explored. The
five-field combination used in the paper were found to perform the best.
\medskip

Using more robust angles could enable smaller margins with range- or
dose-guidance methods to identify dose deterioration. Furthermore, smaller
margins enable better sparing of the OARs.  \nameref{PaperRobust} showed that an
isotropic margin of $5$ mm would be sufficient for target coverage.  However,
isotropic margins are no longer considered a best practice.  Modern \ac{TPS}s
instead take some uncertainties into account using isocentre shifts during
optimization\cite{VanderVoort2016, Fredriksson2011, Eclipse13}.  In current
practice, margins and uncertainties are intentionally overestimated to avoid
under-dosage. A comprehensive method for quantitatively monitoring patient
anatomy could improve cancer outcomes by ensuring target coverage and capturing
errors that humans' could fail to notice\footnote{"To err is human" ---
Proverb}. These tools are not meant to replace the existing verification
methods but to supplement them.\medskip

Three of the papers in this thesis investigate the same group of patients,
although in different ways. This limitation may have biased the papers towards
conclusions that would not be applicable outside these patients. The papers
mainly introduce methods that should be universally applicable. They are not
intended to conclude, e.g.\ which angles to use, but rather to provide methods
for finding them.\medskip

Patients are inherently individual, and it is dangerous to assume otherwise
without verification. For the same reason, this thesis has investigated methods
for verification on an individual daily basis.  Using the same patients for
more studies also had the benefit of having fewer variables.  This
simplification allowed for a deeper dive into the causes of differences between
approaches.\medskip

The methods and tools introduced in this thesis were intended to be universally
applicable. This thesis is not intentionally prostate-specific. However,
similar datasets for other anatomical sites was not obtainable. Thus, future
studies should explore the application of these methods on other anatomical
sites.\medskip

Other recent studies have also developed methods that use the \ac{CBCT} and
\ac{pCT} for immediate plan re-evaluation and potential
adaptation\cite{Veiga2016a, Thing2016, Kurz2015, maspero_single_2020,
zhang_scatter_2020}, for example, fast online
re-optimization\cite{Unkelbach2016}.  \nameref{PaperCBCT}, the paper it is
based on\cite{Park2015a}, and the methods by Thing et al.\cite{Thing2016} and
Zhang et al.\cite{zhang_scatter_2020} are built on the actual physics of the
problem in their approach to simulate scattering.  Maspero et al.\ used a
neural network made for making horses look like zebras in photos. Specifically,
they used an unpaired image-to-image translation cycleGAN, "pix2pix"-style,
net\cite{zhu_unpaired_2018}. Where unpaired means images from each set are not
matched. Thus, CBCT images could be trained against CT images from other
anatomical sites, patients or even centres. Maspero et al. argue that the
network trained unpaired across anatomical sites performed similarly to
separate networks for each site. This network is not based on any physical
properties or constraints, although it performs well.  Arguably, \ac{DIR}-based
methods could simulate anatomical changes between \ac{pCT} and \ac{CBCT}.
However, these methods are usually only constrained by assumptions such as mass
conservation. These assumptions may not hold, e.g.\ due to bladder filling or
tumour shrinkage. A more sophisticated deformation model would take the actual
physics into account.  Zhang et al.\ introduced a method for using
biomechanical models for 4D tracking of the liver\cite{zhang_4d_2019}.  Models
like that could be created for all \ac{VOI}.  Combining those models with the
\ac{DIR} would allow replacing mass-conservation assumption on a
\ac{VOI}-by-\ac{VOI} basis.  For now, Veiga et al.'s manual
approach\cite{Veiga2016a} is reasonable because it forces manual verification.
In comparison, verifying and performing quality assurance for deep
learning-based methods is difficult.  \medskip

\nameref{PaperCBCT} only used phantom data, which means that new artefacts and
problems could arise when applied to patient data. However, other studies
applied the same method to patient data from another vendor's photon gantry
systems with impressive results\cite{Kim2017, Kurz2016, Kurz2016a}.  Tools used
in other clinical studies\cite{Veiga2016a} could address potential issues,
e.g.\ air/fluid movements or tumour growth/shrinkage.  Applying the \ac{pCT} to
\ac{rawCBCT} deformation onto the structures of the \ac{pCT} could help
auto-segment\cite{Thornqvist2013, Thornqvist2010, Thor2011, Peroni2012} as
needed for these tools.\medskip

Combining auto-segmentation with a fast and reliable proton dose calculation
engine\cite{Jia2012, Tian2015, Qin2016} would make it possible to recalculate
the plan in minutes. This speed-up would open the potential for adaptive
strategies.  We have implemented tools for all this in
CBCTrecon\cite{Park2017}. As mentioned earlier, these include automatic
structure deformation and an interface to the \ac{GPU}-based Monte Carlo
dose-calculation tool, \ac{goPMC}. Additionally, like used by Veiga et al., we
added a tool for manual HU modification.\medskip

Kim et al.\cite{Kim2017} used the original implementation of the a priori
scatter correction\cite{Park2015a}. They improved weekly \ac{CBCT}s for $13$
head and neck patients. The \ac{corrCBCT}s allowed them to compare the
\ac{WEPL} differences to the distal surface of delineated tumours. They were
able to measure the proton dose deterioration throughout treatment with a
range-calculation uncertainty of $2\%$. The \ac{WEPL} comparison method used by
Kim et al.\ is similar to the one we used in \nameref{PaperRobust}. However, we
found the correlation to dose deterioration to be weak for the \ac{LN} volume,
even when separating the left and right sides. This weak correlation was the
reason for expanding the \nameref{PaperWEPL} method and finding a better metric
than mean, \ac{SD} or \ac{RMSE}. We tested the method on synthetic set-ups for
verification. The set-up consisted of simulated water phantoms with introduced
air cavities. Even with the improved method, the \ac{LN}s dose degradation did
not correlate any stronger. This time, however, the reason was different. The
\ac{LN}s target had sufficient dose coverage in all scans; we had employed
lessons from our previous studies:
\begin{itemize}
  \item The choice of angles
  \item Robust optimization in the \ac{TPS}
  \item Bony-anatomy registration\cite{busch_online_2019}
\end{itemize}

Dose-guidance, in contrast to range-guidance, has shown great promise in recent
years\cite{nenoff_daily_2019,matter_intensity_2019,botas_online_2018}.  Botas
et al.\ exploited the fast gPMC dose calculation to adapt plans on a spot-wise
basis\cite{botas_online_2018}. They also used the corrCBCT from the original
CBCTrecon implementation\cite{botas_online_2018}. This approach performs well;
however, it still requires further re-delineation and verification. The
iso-WEPL method of \nameref{PaperWEPL} eliminates the need for re-delineation.
Instead, it offers a quantitative second opinion. The iso-WEPL does not provide
an adapted plan in case of changes but rather an alarm if significant. The
iso-WEPL method is well-suited for a combination with robust planning. In
comparison, the adaptive plan generation approach is better suited for a
plan-of-the-day strategy\cite{botas_online_2018, matter_intensity_2019,
nenoff_daily_2019}.

Other studies have explored automated re-delineation. A common trending
approach is to use \ac{ML}. \ac{ML}-based delineation models are trained on
existing delineation data. Thus, these models will have the same bias as the
input data. Delineation from different doctors varies a lot\cite{bondue_2019},
even within the same set of guidelines, although reduced\cite{vinod_2016}. To be
able to create a good model, "good" must first be defined. The training data
will need to be of "good" quality as well. This data also includes \ac{CT},
\ac{MRI} or \ac{PET} images, or a combination\cite{ren_2021, guo_2019}. The
quality of these images also varies across institutions\cite{ginde_2008}.
Therefore, creating a trustworthy model would require a large amount of
high-quality data. The output of \ac{ML} models would, therefore, still need
quality assurance checks. This process can be time-consuming. A recent study
estimated the time from imaging to treatment to be 15 minutes due to manual
correction and quality assurance\cite{rigaud_2021}. Less than 4 minutes would
be spent in the automated segmentation and plan generation\cite{rigaud_2021}.

\nameref{PaperRobust} is the second study to investigate the robustness of many
beam-angles towards inter-fractional motion. The first study by the same
authors\cite{Andersen2015c} used only three patients and mainly served as a
proof of concept. \nameref{PaperNTCP} compares proton and photon plans. The
comparison is not new, but the target and focus are. Only very few studies have
investigated using proton therapy for prostatic lymph nodes. This study was the
first simulated approach that also took beam angle configuration into account.
\nameref{PaperCBCT} builds on an existing method. This study sought mainly to
verify this method against new data. Notably, it used data from a different
CBCT vendor. The study also examined clinical reconstruction algorithms not
previously compared to this implementation.  Finally, \nameref{PaperWEPL}
introduces a new method for \ac{WEPL} calculation. One other study investigated
a method for reverse WEPL calculation\cite{gorgisyan_impact_2017}. However,
this method is the first to reverse-calculate WEPL from structures into a
second scan. Furthermore, \nameref{PaperWEPL} correlates the resulting new
iso-WEPL similarity metrics against dose coverage.
