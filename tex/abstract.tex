\chapter{Summary}

Proton therapy can better spare healthy tissue around a tumour compared to
conventional X-ray radiotherapy (RT).  However, the proton dose distribution is
sensitive to density changes in the beam path.  Robust planning can be used to
account for potential changes ahead of treatment. Additionally, it is necessary
to monitor the patient anatomy throughout treatment.  A combination of these
two strategies may allow treating a more comprehensive range of cancer
indications.\medskip

Patients with advanced prostate cancer receive radiation to a large target,
i.e. the lymph nodes, seminal vesicles and the prostate. Given this extensive
volume, proton therapy could reduce normal-tissue dose significantly, but only
if the uncertainties caused by anatomical changes can be adequately mitigated.
These changes include bowel movement, filling and emptying the bladder, weight
loss/gain, and relative movement of the femurs. Robust planning seeks to create
dose plans that are less affected by inter-fractional or even intra-fractional
changes.\medskip

A computed tomography (CT) can be used as a representation of the density in
the patient. This representation is derived from the attenuation of x-rays. The
stopping behaviour of protons is inherently different, which is also their main
advantage. However, this means the densities measured by X-rays will have to be
translated into approximate proton stopping power values.\medskip

Cone-beam (CB) CTs can be used to verify patient positioning and monitor
anatomical changes. However, due to the scattering of the X-rays in the
patient, the CBCT will contain artefacts. These artefacts result in false
density estimates and cannot be relied on for range and dose
calculations.\medskip

This thesis aims to provide methods and tools to take advantage of robust
planning, image-, range- and dose-guidance strategies.\medskip

In \textbf{Paper \RNum{1}}, we investigated a method for finding robust angles.
The method was applied to 18 advanced prostate cancer patients. Water
equivalent path length (WEPL) ranges and simple one-field proton plans to the
pelvic lymph nodes (LN) were calculated from all around for each patient. These
plans were optimized on the planning CT (pCT) and recalculated across 6-10
weekly repeat CTs (rCT). We correlated angles and patients against WEPL and
dose to find patterns in subpopulations.  We found two generally robust beam
angle configurations from this study: Anterior oblique and posterior gantry
angles.\medskip

The anterior oblique field configurations were investigated in \textbf{Paper
\RNum{2}} and compared to a proton plan with lateral opposing fields and a
photon-based Volumetric Modulated Arc Therapy (VMAT) plan. These plans were
optimized, on the pCT, to spare organs at risk (OAR), and in particular, the
bladder and rectum. Furthermore, these plans were recalculated on 6-10 rCTs
extracting normal-tissue complication probabilities (NTCP) and dose metrics for
analysis. It was found that the proton plan with laterally opposed beams
performed best when considering dose coverage of the prostate CTV. The proton
plans gave a lower mean dose to the OARs. However, the gEUD and NTCP values
were similar to those for the VMAT plans. The rectum and bladder NTCP values
showed a strong correlation with the PTV overlap with the respective OAR.
\medskip

In the first two papers, rCTs were used to evaluate dose and range through the
treatment course. The gantry mounted CBCT can represent the patient anatomy at
treatment time accurately. However, it is not reliable for calculating the dose
and range without mitigating the x-ray scattering effects. In \textbf{Paper
\RNum{3}}, we adapted and optimized a previously introduced method for scatter
correction. We compared the clinical CBCT reconstructions methods and assessed
the improvement and correctness in CT values, proton dose, and range
calculations. We found that the scatter correction significantly improved the
accuracy of the CBCT compared to the clinical reconstructions for both the
photon- and proton-gantry CBCTs.\medskip

\textbf{Paper \RNum{4}} introduced a new method for estimating dose
deterioration based on WEPL ranges. This paper used a combination of the
posterior fields, which were found robust in \textbf{Paper \RNum{1}} for the
LNs, and the laterally opposed fields for the prostate from \textbf{Paper
\RNum{2}}. The WEPL method was integrated into the software application
developed for \textbf{Paper \RNum{3}} to calculate and visualize a
range-shifted estimation of the isodoses swiftly. This method's advantage is
the limited amount of input necessary, only a pCT, the field-specific isodoses
and an rCT (or corrCBCT), thus eliminating the need for re-delineating volumes.

A collection of different approaches to account for inter-fractional motion in
proton therapy has been investigated. Specifically, analyzing beam angle
robustness to dose and ranges and providing fast and reliable methods for
monitoring the influence of inter-fractional changes on the dose plan.
