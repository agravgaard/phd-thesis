\chapter{Background}

Impermanence (Pali: Anicca) is the philosophical problem of \textit{change}.
It is one of the main foundations for existence and the eternal need for
adapting to the present. Humans are ever-changing on the inside and outside.
From the perspective of mutations in the \ac{DNA}, this both means the cause
for evolution and development and cancer. This thesis will focus on the changes
in the scope of inter-fractional radiotherapy, i.e.\ anatomical changes
affecting the treatment.

This thesis is written primarily with medical physics readership in mind. At
the same time related professions and disciplines (e.g.\ radiation oncology and
computer science) should be able to follow the main principles. The intention
is also to provide enough information for further developing the tools and
methods of this thesis.

\section{Cancer}

Cancer comes in a practically endless number of forms. Cancers are often
classified based on specific traits of the particular tumour
\cite{hanahan_hallmarks_2011}.  One tumour usually contains many cancers due to
most cancers' mutational and highly proliferative nature. However, they are
most often treated as one.  More recent approaches to individualized medicine
on a molecular level are attempting to target these issues, but that is out of
scope for this thesis.\medskip

The traits of malignant cancer cells often include resisting apoptosis,
accelerated or unregulated mitosis, and increased invasive motility. Malignant
cancer cells also often consume more energy for proliferation, possibly due to
other malfunctions within the cell or its signalling receptors. The
\textit{cure} for cancer would, therefore, be to remove or destroy these cells.
Removal is the surgical method; destruction can happen directly or by causing
apoptosis. The cell has many built-in mechanisms for committing apoptosis; most
are chemically triggered. Cancerous cells resisting apoptosis are immune to
some apoptosis pathways.\medskip

Radiation therapy uses a more direct way to cause apoptosis by creating free
radicals or direct \ac{DNA} damage. While a single free radical or \ac{DNA}
strand break is unlikely to do much damage, radiation therapy may cause $30$ to
$60$ double-strand breaks per cell per Gray (Gy) dose (for 200 kVp x-rays)
\cite{Mori2018}.  Furthermore, cancer cells are more likely to be defective in
the repair mechanisms.  However, photon-based \ac{RT} mainly creates free
radicals from secondary electrons and oxygen in the nucleus to cause the strand
breaks. Thus, cancer cells depleted of oxygen, i.e.\ hypoxic, will be resistant
to radiation damage. Particle therapy is, therefore, of increased interest for
hypoxic tumours. Heavier ions like $^{12}C$ are particularly effective compared
to protons with regards to hypoxic tumours \cite{malinen_2015}.\medskip

The methods for treating cancer include surgical, by removing the tumour
physically, Chemical, i.e.\  chemotherapy, targeting common traits of cancerous
cells, \ac{RT}, which aims to kill the cancerous cells with radiation damage.
These methods are usually used in combination to give a more effective
treatment. An individual patients treatment might omit one or more of these
methods to avoid potentially severe side effects.\medskip

In recent years, particle and particularly \ac{PT} have gained increasing
attention. It has similar or increased cancer-killing potential compared to
regular photon-based \ac{RT} but with an extra benefit of reduced normal-tissue
dose.  Particle therapy benefits from the Bragg peak (\fref{fig:bragg}); this
means that the particles will deliver their energy through particle
interactions before certain depth into the matter and almost nothing after that
depth. This effect is beneficial for cancer therapy.  By tuning the protons'
energy, it is possible to make them stop and deliver their energy, i.e.\ dose,
inside tumours with practically no dose on the distal side.  Furthermore, it is
possible to create a volume with uniform dose, covering the entire tumour
volume, by combining layers of Bragg peaks of different energies into a
\ac{SOBP} (\fref{fig:bragg}).\medskip

\input{tex/figures/bragg}

Protons, and notably heavier particles, also have increased effectiveness,
i.e.\ \ac{RBE}, compared to photons. Moreover, this effect is even more
pronounced in cancer cells as they often have a reduced ability to repair the
\ac{DNA} damage caused by these particles \cite{Kramer2000}.\medskip

\section{Photon therapy}

Photon-based \ac{RT} combines x-ray fields from different directions to create
a focal point of high intensity in the tumour \cite{Otto2008}. A photon-based
radiotherapy gantry can typically deliver two energies in the range of $4$ to
$20$ MeV; one low and one high. Modifying the energy will change the dose-depth
curve. Higher energies will give a higher dose at depth, while lower energies
will let the dose curve peak earlier \cite{AttixChapter9}.

\subsection{Photon interactions}

For the energies in regular \ac{RT}, mainly two types of interaction happen;
Compton scattering and pair production. The latter is increasingly prevalent at
higher energies. Compton scattering is when the photon hits an electron and
transfers energy to the electron, giving the electron momentum
(\fref{fig:compton}). The photon is absorbed near a nucleus in pair production
to create an electron-positron pair (\fref{fig:pairprod}).  The resulting
electrons and the positron from both interactions, i.e.\ secondary beta
particles, are considered effective in cancer treatment. These particles will
act as ionizing radiation and create free radicals in the cells, which will
likely damage cells and cause apoptosis.

\input{tex/equations/compton}

\input{tex/equations/pairproduction}

\subsection{Technology}

Photon-based RT delivery is a mature and well-optimized technology, and it
keeps improving. The method for delivering the treatment beam is consistent
across vendors by using linear accelerators with tuned-cavity waveguides,
accelerating electrons by pushing/dragging them forward on a Radio Frequency
(RF) wave. The electrons are then hurled into a material such as Wolfram to
produce the x-rays through Bremsstrahlung. Due to the scattered nature of
Bremsstrahlung, the material, its shape, surrounding materials and downstream
materials and their shapes are optimized to shape the beam and tune the x-ray
energy distribution. Furthermore, various methods for collimating the beam to
the shape of the target is used along with sophisticated methods for optimizing
treatment plans in software to combine fields from many directions with
different intensities, i.e.\ \ac{IMRT}.

\section{Proton therapy}

Particle therapy is still in its infancy, despite having existed for a while.
Firstly, until 2006 there were less than 20 particle therapy centres in
operation worldwide; now, there is more than 100 \cite{noauthor_ptcog_nodate}.
Secondly, the Bragg peak of protons is inherently too sensitive to density
changes relative to the errors/uncertainties in the treatment process
\cite{unkelbach_robust_2018}.  This thesis will focus on the second
problem.\medskip

\ac{PT} takes advantage of the \ac{SOBP}, as mentioned earlier, to reduce the
normal-tissue dose. Modern photon-based \ac{RT} uses many-field plans to create
a higher dose where the fields overlap.  However, this many-field approach
means that a large volume will receive a dose, although it may be small.
Therefore, \ac{PT} is particularly advantageous in children's cases because
even low doses may cause long-term side effects. In particular in regions with
critical organs sensitive to low-dose radiation, which is more challenging to
avoid with photon-based \ac{RT}\cite{Dinh2012, Langendijk2013}.\medskip

\subsection{Particle interactions}

\ac{PT} relies mainly on direct \ac{DNA} damages to cause apoptosis.  The
interactions include ionization, excitation, electron capture/loss and elastic
scattering\cite{alcocer_proton_2019}. The proton interactions are often
characterized as hard ($b \approx a$), soft ($b > a$) and radiative ($b < a$)
collisions. Here $b$ signifies the protons distance from an interacting atomic
nucleus and $a$ the atomic radius. Soft collisions dominate, while radiative
interactions are almost negligible due to proton therapy's relatively low
energies, typically $\in [70, 250]$ MeV, and their high mass relative to
electrons.\medskip

The protons' energy loss as it travels through matter is not deterministic but
rather a statistical process. Bohr introduced this concept as "energy
straggling". Commonly, the loss of energy per distance, i.e.\ stopping power,
is approximated by the Bethe equation \eqref{eq:Bethe}, valid for protons with
energies above $\approx 1$ MeV.

\input{tex/equations/Bethe}

Here the key variables $Z$, $A$, $\rho$ and $I$, respectively, describe the
charge, atomic number, density, and mean excitation energy of the target
material, and $\beta$, which is the protons' speed in units of the speed of
light. This equation makes it possible to estimate the range of protons by
integrating the inverse stopping power from $0$ to the proton's initial kinetic
energy.

\subsection{Technology}

\ac{PT} has been implemented in various ways. Firstly, the method for
delivering a conformal dose distribution. Currently, the options are either
spot-scanning or passive scattering. Spot scanning, also referred to as
\ac{PBS}, delivers the dose in spots positioned (x \& y in
\ac{BEV}\footnote{The z-axis is not defined for spot scanning in the BEV
coordinate system, it's instead given by the energies for each weighted spot.
See (300A, 0394) in the DICOM standard:
\url{https://dicom.nema.org/medical/dicom/2016a/output/chtml/part03/sect_C.8.8.25.html}})
by electromagnets, usually two dipoles. In contrast, passive scattering
collimates a scattered broad field instead.  Spot scanning has the advantage of
creating a more conformal distribution on the proximal side of the
target.\medskip

Meanwhile, passive scatter shapes to the distal side only, usually using a
patient-specific mould in the aperture. However, passive scattering has the
advantage of being more robust to inter-play effects, i.e.\  organ motion
during treatment. The spot scanning method requires some time to scan over all
the delivery points.  Contrarily, passive scattering can deliver the whole
distribution at once (depending on how the energy layering for the \ac{SOBP} is
performed). While spot-scanning technically allows placing spots anywhere in
the \ac{BEV}s coordinate system, they are usually placed in a grid by the
\ac{TPS}. Similarly, the \ac{TPS} will also need to enable variable energy
layering if the specific beamline allows it \cite{doolan_comparison_2015}.

Secondly, the energy selection for creating the \ac{SOBP} and reaching
different depths can be made various ways. Since the \ac{SOBP} energies may not
vary much, the most straightforward option, typical for passive scattering, is
to put a wheel in the beamline with an angular difference in thickness. By
rotating the wheel, each thickness's width will degrade the beam's energy
according to that energy's weight in the \ac{SOBP}. However, different wheels
will be needed for different depth placement of the \ac{SOBP} and different
\ac{SOBP} plateau lengths. Another energy selection method commonly
placed after a cyclotron is to use a wedge degrader, i.e.\ two opposing series
of wedges adjusted for degrading the beam's energy as desired.\medskip

Thirdly, there is the option of having a fixed beamline (usually a horizontal
beam) or having a rotating gantry that can bend the beam to different angles
(usually 180 or more degrees of rotation). With both options, the patients can
also be positioned as appropriate for the treatment requiring more creativeness
for the fixed beam direction.\medskip

Designing the specific beamline gives many options for tuning and shaping the
beam (\fref{fig:beamline}). This spectrum of options will usually boil down to
either being called spot-scanning or passive scattering.  Sometimes with a
mention of whether a cyclotron, synchro-cyclotron or synchrotron was used. Most
of this thesis will apply to all options, although it will assume having a
rotating gantry with 360 degrees of rotation. \nameref{PaperNTCP} and
\nameref{PaperWEPL} will only apply to spot-scanning as they used robust and
multi-field optimization methods only compatible with spot-scanning.
\nameref{PaperRobust} and \nameref{PaperCBCT} used spot-scanning based dose
calculations.  However, as they only considered single fields without robust
optimization, the results should also apply to passive scatter based
\ac{PT}.\medskip

\input{tex/figures/beamline.tex}

\subsection{Efficacy}

\ac{PT} is being introduced to an increasing number of patient groups and
tumour sites. The established indications include mainly childhood cancer and
selected brain tumours. However, these represent only a minor fraction
($2-3\%$) of the patients treated today with \ac{RT}\cite{Langendijk2013}.  New
indications are continuously being investigated, given the evident potential to
spare normal tissue and improve clinical outcomes for $10$ to $15\%$ of \ac{RT}
patients \cite{Widesott2011, Muren2013}.  The tumour sites representing
potential new \ac{PT} indications in the thorax, abdomen and pelvis
\cite{Langendijk2013} share a mutual feature. The precision of treatment here
is challenged by intra- and inter-fractional organ motion, varying across sites
and between patients \cite{Engelsman2013}.\medskip

With \ac{IMPT}, a high and uniform dose across the tumour volume can be
obtained \cite{lomax_intensity_1999}, illustrated by the \ac{SOBP} in
\fref{fig:bragg}. However, given the proton's range sensitivity to density
changes \cite{Engelsman2013}, the target and surrounding tissues are at risk of
not receiving the planned dose; unless appropriately accounting for the
uncertainties in the range \cite{Chen2013, Park2013, Unkelbach2007,
Unkelbach2009}.

\section{Computed tomography}

A \ac{CT} is a back-projection of x-ray projections into physical 3D space
representing an object's electron density through attenuation coefficients.  In
the case of cancer therapy, the object is usually a human. The detector array
is thin ($< 5$ mm) for most \ac{CT} scanners \cite{Goldman57} and rotates
around the object opposite the x-ray source.  The thin fan-beam geometry
reduces the number of scattered photons absorbed in the detector.  While in the
case of \ac{CB} \ac{CT}, scatter becomes a more significant problem
(\fref{fig:Artifacts}) as the array is rectangular (length, width $\in [15, 30]$
cm).  Only one full or half rotation is usually performed because the \ac{CB}
scanner is mounted on the treatment gantry, which does approximately one
rotation per minute. Both \ac{CT} and \ac{CBCT} scanners often use anti-scatter
grids in front of the detector array to reduce scatter.\medskip

\input{tex/figures/artifacts.tex}

A \ac{CBCT} can adequately acquire the patient's anatomy at treatment and in
the treatment position despite the increased scatter.  The wide \ac{CB} sensor
array also gives a better resolution in the \ac{SI} direction than a \ac{CT}
scanner. The \ac{CT} scanner records data to reconstruct one \textit{slice} for
every $\approx 3$ mm step. Lowering the step size to increase resolution would
require a longer scan time --- making \ac{CB} an attractive option for tracking
changes during treatment as well.\medskip

\input{tex/figures/interact_contrib.tex}

The scatter in \ac{CT} and \ac{CBCT} scans are primarily from Compton
scattering and the photoelectric effect (\fref{fig:photoelectric}). Compton
scattering dominates at higher energies. The photoelectric effect dominates at
lower energies. Rayleigh scattering is also present at lower energies but is
never dominant (\fref{fig:interactcontrib}). The scattering is dependent on the
scattering angle $\Theta$ (\fref{fig:compton}). Even assuming a Gaussian
distribution for $\Theta$, the problem is still too complicated to be solved
using Richardson-Lucy deconvolution \cite{lucy_iterative_1974, Richardson1972}.
Every scattering point may have a diverse spread of the $\Theta$ distribution
dependent on the matter at the interaction point and all matter before
that.\medskip

\input{tex/equations/photoelectric}

For regular photon-based \ac{RT}, well-calibrated \ac{CBCT} images can give
sufficient information for dose guidance as both the image and treatment are
based on photons.  However, for protons, even the correctness of a \ac{CT}
image is questionable when calculating stopping power. However, for the papers
in this thesis, it is assumed that the \ac{CT}'s electron density map is
directly transferable to the stopping power of protons \cite{Schaffner1998}.
The transformation to stopping power is handled through linear interpolation of
a simple \ac{LUT}, i.e.\ a HU (key), stopping power (value) key-value
table.\medskip

\ac{CT} images are essentially a 3D matrix. The indexed location gives each
voxel's physical position when multiplied by the voxel size in x and y
directions and the slice thickness in the z-direction plus an offset. The value
of the voxel is a relative attenuation coefficient which is linked to the
relative electron density at that point in \ac{HU} given by \eqref{eq:HU}:

\input{tex/equations/HU}

Where $\mu$ is the linear attenuation coefficient, given by reconstruction of
the measured attenuation into a voxel-cube.  Similarly, $\mu_{\text{water}}$
and $\mu_{\text{air}}$ is the linear attenuation coefficient for water and air,
respectively.\medskip

The back-projection in \ac{CBCT} reconstruction works roughly by tracing along
x-ray paths. From every pixel in the projection and towards the x-ray source
position for that projection into a 3D cube (\fref{fig:BP}).  Before the
back-projection, the projections are usually filtered in the Fourier space,
with a \textit{Ram-Lak} filter multiplied by some function, e.g.\ Hann, Hamming
or Cosine.  The filtered back-projection method is referred to as a \ac{FDK}
reconstruction\cite{Feldkamp1984}.  The \ac{FDK} algorithm has been implemented
in most reconstruction software, e.g.\ \ac{RTK}\cite{Rit2014} and
Plastimatch\cite{Sharp2007}, which have implementations in C++, \ac{CUDA} and
\ac{OpenCL}. The inverse process, forward-projection, generates a \ac{DRR},
which simulates the \ac{CB} projections from a \ac{CT} image.

\input{tex/figures/FBP}

\subsection{Scatter correction}

Dose guidance requires the \ac{HU} values to be reliable, particularly for
\ac{PT}. With the high amount of scattering in \ac{CB} projections causing
artefacts in the reconstructed \ac{CBCT}, the \ac{HU} values are unreliable
(\fref{fig:Artifacts}). The \textit{a priori} scatter correction method
proposes using the \ac{deformCT} to estimate scattering and subtract it from
the \ac{CB} projections\cite{Niu2010, Niu2012}.  Park et al.\ implemented this
algorithm\cite{Park2015a}, as illustrated in \fref{fig:flowgraph}.  It takes
the \ac{CB} projections and a \ac{pCT} as input, the \ac{pCT} being the
\textit{a priori} information. It was later shown by Z{\"o}llner et al.\ that
this method also accounts for beam hardening\cite{Zollner2017}.\medskip

\input{tex/tikzfigs/flowgraph}

Thing et al. presented a similar method \cite{Thing2016}. Instead of deforming
the \ac{CT} to the anatomy at every \ac{CBCT}, they simulated the scatter only
on the original \ac{CT}. They simulated the scatter of the specific \ac{CBCT}
scanner on the planning \ac{CT} using Monte Carlo calculation. This method may
produce higher quality scatter maps and take significantly less time to perform
at treatment \cite{Thing2016}.  However, the use of the planning \ac{CT}
without deforming to the anatomy of the day may produce overcorrections in the
presence of anatomical changes that could influence proton range. Note, this
method was developed for \ac{IMRT}, not particle therapy.\medskip

Zhang et al. \cite{zhang_scatter_2020} made a Monte Carlo particle path-based
simulation engine for \ac{CB} scatter that runs on the \ac{GPU}. With this
\ac{GPU} engine, they combined the ideas of the above methods to scatter
correct with high quality scatter maps at treatment time
\cite{zhang_scatter_2020}.

\section{Treatment planning}

\subsection{Volumes of Interest}

It is necessary to know which organs or regions to radiate for treatment
planning and which should be avoided. Therapists delineate all \ac{VOI} in a
volumetric image, usually \ac{CT} or \ac{MRI} scans.  Each \ac{VOI} is named as
the organ it represents or according to guidelines for regions, e.g.\ tumours.
The \ac{GTV} outlines the tumour itself. The \ac{GTV} may be removed by
surgery, and \ac{RT} is then given to the region with a remaining risk of being
cancerous.  \ac{VOI}s are created by delineating a contour in each slice where
the \ac{VOI} is present. A contour is a list of points corresponding to the
physical positions enclosing the region on each slice.\medskip

A \ac{CTV} represents the region that has a risk of being cancerous.  The
\ac{ITV} is supposed to account for setup uncertainty and movements, such as
respiratory, setup and inter-fractional changes. The \ac{PTV} is typically
created by adding a margin to the \ac{ITV} or \ac{CTV} to ensure the whole
\ac{CTV} receives the prescribed dose. A \ac{RTV} is similar to a \ac{PTV} but
is generated by the \ac{TPS} for each field using uncertainty parameters. The
physicist creates the \ac{PTV} or \ac{RTV} during the dose planning with the
corresponding margin configuration (\fref{fig:TVs})\cite{Podgorsak2006}.
\medskip

As the name suggests, \ac{OAR} are sensitive to dose, i.e.\ could lose
function or develop secondary cancers or other side effects. For example, for
the pelvic region, side effects may include diarrhoea, rectal bleeding,
incontinence, bladder irritation, and sexual dysfunction. \medskip

\input{tex/figures/TVs}

\subsection{Treatment planning systems}

In \ac{PT}, the \ac{TPS} adds intensity weighted Bragg-peaks until the target
is covered\cite{Podgorsak2006}.  It uses the \ac{CT} values translated to
stopping power ratios to calculate the energies necessary to achieve each
Bragg-peak layer's (and spot's) range.  A proton beam's axial profile is
usually assumed to be approximately symmetric and Gaussian shaped. The \ac{TPS}
calculates the dose from a vendor-specific model. As an example, \ac{TRiP} uses
\eqref{eq:KramerDose} \cite{Kramer2000_physical}.

\input{tex/equations/KramerDose}

Equation \eqref{eq:KramerDose} describes the dose from a single ion beam, with
energy $E_{\text{beam}}$ centred at $(x_0,y_0)$. This model does not take
\textit{multiple scattering} and particles ($\gamma$ and $\delta$-rays) created
by the slowing down of ions into account unless $\sigma$ is made
depth-dependent.  $r$ is the lateral distance from the beam's centre. $\sigma$
is the width of the Gaussian beam profile. $N$ is the total number of
particles.  $d(E_{\text{beam}},z)$\eqref{eq:Kramerd} is the energy loss
distribution as a function of depth, $z$ \cite{Kramer2000_physical}.

\input{tex/equations/Kramerd}

With the contents of the integral being the fluence of the beam, times the mass
stopping power, with $T$ describing a particular particle species.\medskip

Multi-field plans can generally be optimized in two ways. The more
straightforward \ac{SFUD} method optimizes each field individually for a
homogeneous dose distribution covering the target. \ac{MFO} methods optimized
multiple fields at once for the combined dose to cover the target uniformly.
The particular field in \ac{MFO} may have a very heterogeneous dose
distribution. \ac{MFO} has more freedom to avoid \ac{OAR}s. However, the
individually heterogeneous doses are more likely to combine in hot and cold
spots when anatomical changes happen. \ac{SFUD} can, in that way, provide a more
robust uniform dose, but potentially at the cost of more dose to
\ac{OAR}s.\medskip

Robust optimization can be used with both multi-field optimization strategies.
However, only one of the optimization methods may be implemented in the
\ac{TPS}. The \ac{TPS} handles robust optimization and can usually only be
adapted by uncertainty parameters. These parameters may include range
uncertainty as a percentage and geometric shift. Some systems may also allow
adding additional scans to the optimization.

\section{Dose evaluation}

\subsection{Dose prescription and Dose Volume Histograms}

When clinical dose plans are made, they must follow guidelines that define
certain dose constraints for the given indication. These constraints must be
met for the plan to be approved. A typical constraint tells that a certain
percentage of the volume of a specific \ac{OAR} maximally can receive a certain
amount of dose. That amount is either in Gy or in a percentage of the
prescribed dose to the tumour target. The target has similar constraints,
additionally defining the minimal dose values, homogeneity index and similar
metrics. \medskip

A \ac{DVH} is used to analyze and visualize these constraints. The dose as a
percentage ($D_{\%}$) or in Gy is plotted against the percentage of the volume
($V_{\%}$) receiving this dose. \ac{DVH}s are most often displayed as
cumulative plots \cite{Podgorsak2006}.  \ac{DVH} curves are calculated using
the delineations of the volumes and the dose distribution generated by the
\ac{TPS}.

\subsection{NTCP}

\ac{NTCP} is a metric based on how patients have responded to treatment in the
past. For a given \ac{OAR} receiving a dose, previously treated patients either
had or did not have a complication, e.g.\ side effects.  Assuming a correlation
between the dose the organ received and the complication, a model
\eqref{eq:NTCP}\cite{lyman_complication_1985} can be fitted to estimate the
probability that a new patient would have the same complication, given the dose
to that \ac{OAR}.

\input{tex/equations/NTCP}

The \ac{gEUD} \eqref{eq:gEUD} is weighted by the $1/n$ factor, which is tissue
specific. In that way, \ac{NTCP} can be adjusted to be more or less sensitive
to a higher dose. A low $n$ means hot spots matter more, e.g.\ for serial
organs.  Inversely, a high $n$ means the volume receiving dose matters more.
$\text{TD}_{50}$ is the tissue-specific median toxic dose given a uniform dose
to the whole volume. $m$ is a measure of the slope of the sigmoid curve
represented by the integral of the normal distribution\cite{Semenenko2008}.

\section{Treatment verification}

\subsection{Image- and Dose-guidance}

Throughout the \ac{RT} treatment, the patient is likely to have anatomical
changes that influence the delivery of the planned dose.  These changes could
be anything from breathing and heartbeat to bowel movements or weight loss. As
tumour shrinkage or growth is expected, all cancer sites may be subject to
changes in the patient that could influence treatment. Monitoring these changes
requires knowledge of the differences from the time of planning. Planning is
performed on a \ac{pCT}; this image, therefore, usually serves as the base.
There are several options for acquiring metrics that measure the difference
from the base, including \ac{CB} and \ac{MRI} integrated with the treatment
gantry, 2D and 3D cameras that track the surface of the patient, and in-room
\ac{CT}. Common for these methods is that they rely on image registration.
Rigid image registration can be used to align the patient for treatment using a
movable couch. The registration is typically visually inspected.\medskip

\ac{DIR} of the \ac{pCT} to the day's image can give an image that may be more
accurate in \ac{HU} values.  This \ac{deformCT} can then be used for further
investigation, e.g.\ dose-guidance.  Dose-guidance simulates the dose-plan from
the \ac{pCT} on an image of the day or the \ac{deformCT}. This method can be
extended to be adaptive dose guidance by re-optimizing the dose-plan when
necessary.  However, current workflows for adaptive dose guidance are
cumbersome as many steps have to be taken to approve the new
plan\cite{Veiga2016a}. Before those steps are completed, the patient anatomy
may have changed again.

\subsection{Water equivalent path length}

\ac{WEPL} is used to estimate particle ranges, i.e.\ the equivalent range of a
particle in water. For proton therapy, the \ac{CT} image would be translated
into proton specific stopping power values. The \ac{WEPL} is calculated as a
line-integral through the resulting \ac{CT}. \ac{WEPL} can then be used to
estimate the impact of anatomical changes throughout treatment on the plan. The
simplicity of \ac{WEPL} calculation compared to dose calculation makes it a
compelling metric for quick estimation of changes to dose coverage without the
requirement for re-delineation of organs and targets.\medskip

\ac{WEPL} has been used in several studies to estimate proton range and by
proxy dose degradation \cite{Kim2017, kim_beam_2020, Casares-Magaz2014b,
gorgisyan_impact_2017, Andersen2015c}. Casares-Magaz et al. showed how
\ac{WEPL} differences and target dose degradation follow a similar pattern
\cite{Casares-Magaz2014b}.  However, the similarity is not an exact match.
\ac{WEPL} does not consider the accumulated dose from combining proton beamlets
as in the \ac{SOBP}, only the peak position.

\subsection{GPU-based proton dose calculation}

With the ever-changing patient anatomy, dose-guidance methods must be fast to
capture the patient anatomy before it changes again.  These methods should
allow adaptation, e.g.\ moving the couch, or treatment decisions, e.g.\ if a
new plan is needed, without excessively impacting the time from imaging to
treatment. For dose-guidance methods, there are mainly two bottle-necks,
re-delineation of \ac{VOI}s and dose re-calculation. Novel approaches to dose
calculation using the power of the \ac{GPU} has brought down the time for
accurate Monte Carlo based dose calculation to the degree that will enable
dose-guidance workflows\cite{Qin2016, Unkelbach2016,
botas_online_2018}.\medskip

Monte Carlo based dose calculation is a good candidate for offloading to the
\ac{GPU}. This calculation is a massively parallel process; each particle track
can be handled independently. The dose cube would require some mutex-like
control, i.e.\ atomic addition. However, the lock can be set per voxel, so
writing the dose would not lock all other threads.  Moreover, it would require
a random number generator that works across threads on a GPU. However, there
exist several implementations of these, e.g.\ "cuRAND", "rocRAND", and
"clRAND".  The random number generator will simulate interactions, using
empirical cross-section data, along the particle path until the resulting
particles' energy is under a low threshold.

\subsection{Beam angle robustness for inter-fractional motion}

\textit{Beam angle robustness} is a treatment strategy that takes advantage of
the angular and spatial distribution of anatomical changes relative to the
tumour to potentially find beam directions that would see less change either
intra- or inter-fractionally.\medskip

Previous studies of beam angle evaluation/optimization for plan robustness have
explored primarily intra-fraction motion/uncertainties quantified using 4D-CT
scans \cite{Casares-Magaz2014b, Bernatowicz2013, Bernatowicz2015} or
simulations of setup uncertainties by isocentre shifts \cite{VanderVoort2016}.
Similarly, modern \ac{TPS}s use isocentre shifts and range uncertainty
parameters to optimize proton plans robustly. Some may have the option to add
more CT images to the optimizer. However, only a limited set of studies has
investigated inter-fractional motion for prostate cancer for proton therapy
\cite{Andersen2015c,Thornqvist2013a,Soukup2009}.\medskip

Thörnqvist et al.\ \cite{Thornqvist2013a} investigated four patients from the
same population as explored in this thesis. They explored the inter-fractional
robustness of \ac{IMPT} for target coverage. Their findings included that a
$10$ mm isotropic margin was not sufficient to achieve target coverage
\cite{Thornqvist2013a}.
