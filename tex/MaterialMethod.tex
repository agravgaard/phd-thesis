\chapter{Materials and Methods}

\section{Patient cohort and image material}

\subsection{Prostate cancer patient data (Paper I, II \& IV)}

The patient data was taken from a patient group previously treated for locally
advanced prostate cancer with \ac{IMRT} (Haukeland University Hospital, Bergen,
Norway). Each patient's dataset consisted of a \ac{pCT} (Prospeed SX Power, GE
Medical Systems, Milwaukee, WI, USA) as well as $7-10$ \ac{rCT} scans. A
medical professional had delineated \ac{VOI}s on each scan. For the three
studies, the investigated subset of the patients was: $18$ patients for
\nameref{PaperRobust} and $8$ for \nameref{PaperNTCP} and \nameref{PaperWEPL}.
The eight patients were selected randomly, with exclusion only based on
technical difficulties, i.e.\ incomplete, corrupt, unreadable or otherwise
software-incompatible data. Some \ac{rCT}s were missing delineations for the
\ac{OAR}s and were re-delineated for \nameref{PaperWEPL}. The images had a
slice thickness of $[2, 3]~\textrm{mm}$, and each slice was $512 \times 512$
pixels.  Further details and a description of the treatment protocol are
available in previous works \cite{Thornqvist2013a, Thornqvist2011, Muren2008}.

\subsection{Phantom data (Paper III)}

Two phantoms were used: The Catphan 604 and the Alderson phantom; both scanned
in a clinical dual-energy \ac{CT} scanner (Siemens Somatom Definition Edge,
\ac{DCPT}, Denmark). The scan used the “Monoenergetic Plus” option extracted at
$90$ keV, with a split filter ($120$ kV TwinBeam) and $64 \times 0.6$ mm
collimation. The reconstruction used the "Q34s" kernel to reduce beam hardening
with the "Admire" iterative method set to "level 3". These values were the
settings used in the clinic at the time.\medskip

\ac{CB} scans were acquired of both phantoms on both a photon (TrueBeam OBI;
Aarhus University Hospital, Aarhus, Denmark) and a proton (Varian ProBeam
Dynamic Peak Imaging OBI; \ac{DCPT}, Aarhus, Denmark) therapy gantry. Three
sections of the Alderson phantom were scanned: The head, thorax and pelvis.
Details for the specific voltage, current and pulse length settings are
available in \tref{tab:CBsettings}.\medskip

\input{tex/tables/CBsettings}

\ac{CBCT} reconstructions were also generated with the clinical software for
comparison, \ac{stdCBCT}. An \ac{iterCBCT} was also produced with the clinical
software using the vendors' default settings for the photon gantry.

\subsection{Xim Projection file format (Paper III)}\label{sec:xim}

The Varian TrueBeam and ProBeam \ac{CB} systems store projection files in the
same format. The first test projection sets were initially obtained from a
ProBeam gantry (Scripps Proton Therapy Center, San Diego, CA, USA). There were
no practically usable readers for this file format, only a \matl~script and
some
documentation\footnote{\url{bitbucket.org/dmoderesearchtools/ximreader/src}}.
The compression method is identical to the "Hnd" file format used in older
Varian CB imagers. There existed a C++ reader for the Hnd format in \ac{RTK},
so it was used as a template.  The file formats differ mainly in the header,
i.e.\ the meta-data preceding the image data. The header is binary and contains
descriptors for the projection data, such as the projections size, bits per
pixel, and a \ac{LUT} to interpret the bits per value in the compressed
data.\medskip

The decompression of the data is a sequential process by the nature of the
compression method. The value of each pixel depends on the value of the
decompressed previous one.  The \matl~script took 30 seconds to read just one
projection.  Luckily, through the power of bitwise operations in C++ and some
aggressive optimizations, the reading time could be brought down to 2-3 seconds
for 500 projections.  The optimization included computing the complete
compressed data's size and reading it all in one chunk instead of value by
value. Secondly, using a switch-case instead of $2^x$ to translate values ($x
\in {1, 2, 3}$). Thirdly, manual loop unrolling to help the compiler do cache
and \ac{SIMD} optimizations\footnote{Although these steps could seem like
premature optimization, they were all carefully measured on diverse computer
hardware, and the various compilers produced assembly was examined with
\url{godbolt.org}}. The implementation was contributed to \ac{RTK} and can be
seen in the supplementary material (Listing~\ref{lst:XimReader}).\medskip

For a given projection image, $\mathbf{R}$, the compression method can be
mathematically written as:

\input{tex/equations/diff}

This compression method only stores the difference, $d_{i,j}$, between the
pixels\eqref{eq:diff} (and the first row + one pixel, uncompressed). If the
difference is small, it can be stored in 8 bits, larger in 16 bits, and largest
in 32 bits, i.e.\ char, short and long in the \ac{LLP64} used on 64bit
Microsoft Windows.  To determine the bits used to store each difference value,
a \ac{LUT} is used. The table stores the difference value types as two bits.
The \ac{LUT}, therefore, has a size equal to a fourth of the projection image
size.\medskip

The compressed file would have approximately $5/8$ the size, or $37.5\%$ less
for projections with little noise. If half the stored difference values are
larger than $\pm128$ (needing 16 bits), the compressed size would be $7/8$, or
$12.5\%$ less.  Thus, for a more noisy image, the compression would perform
worse. As the projection data is compressed and stored raw, i.e.\ before the
Ramp filter is applied, the projection data will usually be noisy.  JPEG2000
lossless would be expected to achieve the same or better compression ratios
more efficiently and is allowed by \ac{DICOM}. Thus, there is no good reason for
Varian to keep using this old compression method.

\section{WEPL methods (Paper I, III \& IV)}

In \nameref{PaperRobust}, \ac{WEPL} was calculated for each ray path in the
\ac{BEV} to a plane behind the \ac{LN} \ac{CTV}'s distal surface, using the
\ac{WEPL} calculation method in the \ac{PyTRiP} \cite{Andersen2015c}. More
details of the method are available in a previous publication by Casares-Magaz
et al.  \cite{Casares-Magaz2014b}. The \ac{BEV} \ac{WEPL} map was calculated
and averaged for each beam angle configuration, i.e.\ gantry angles $0^{\circ}$
to $180^{\circ}$ for the left \ac{LN} target and $180^{\circ}$ to $360^{\circ}$
for the right, and couch angles from $-90^{\circ}$ to $90^{\circ}$ for both in
$5^{\circ}$ intervals.  These angular average \ac{WEPL} maps were created for
each \ac{pCT} and each \ac{rCT}. The \ac{SD} of the absolute differences
between the \ac{pCT} and each \ac{rCT} were then calculated for further
analysis. Moreover, the mean and \ac{SD} of the \ac{WEPL} differences for a
couch angle of $0^{\circ}$ were extracted for each \ac{CT}.\medskip

The \ac{WEPL} calculation algorithm was rewritten in \nameref{PaperCBCT} from
the C extension in \ac{PyTRiP} to C++ with linear interpolation at each step
between the nearest voxels. Moreover, in \nameref{PaperWEPL}, this version was
extended to include reverse \ac{WEPL} calculation, as seen in the supplementary
material (Listing~\ref{lst:WEPL}).\medskip

In \nameref{PaperCBCT}, \ac{WEPL} was calculated from every point in a grid of
$1$-by-$1$ mm spacing posterior of the couch and towards anterior until the
edge of the given \ac{CT}.  For all reconstructions and \ac{CT}s, the
respective \ac{WEPL} maps were subtracted from the \ac{WEPL} map of the
\ac{rigidCT}, from which the mean and \ac{SD} was calculated. The \ac{rigidCT}
were used as the ground truth because the phantoms used should not have
internal movement.\medskip

In \nameref{PaperWEPL}, points from generated isodose structures were used as
the starting points for the \ac{WEPL} calculations. The isodoses were generated
for each field, so the \ac{WEPL} were calculated in the corresponding opposite
beam direction. The point of intersection with the limits of the \ac{pCT} was
used in the next step for the reverse \ac{WEPL} calculation by stepping into
the \ac{rCT} images in the beam direction until the same \ac{WEPL} value was
achieved, resulting in a WEPL-corrected isodose, or \textit{iso-WEPL}.  Various
options were explored for getting the most dose analogous results.  Using the
whole structure was not a good option as the proximal side would either stick
to the skin surface or possibly have too dramatic isodose changes due to the
flatness of the dose proximal to the \ac{SOBP}. We tried different methods for
extracting the target's distal surface by shifting the structure by a delta in
beam direction and keeping only the original structure points inside the
shifted structure, both with the winding number and the odd-even rule and with
different delta values.  However, we found that using the points distal to the
centre of each "ROI Contour" (name in the \ac{DICOM} standard) gave by far the
most stable results.

\section{Treatment planning and dose calculation}

\subsection{Beam angle evaluation (Paper I)}

Optimized single beam proton dose plans were created for every $5^{\circ}$
gantry angle, with the couch angle $0^{\circ}$. The plans were created through
\ac{PyTRiP}\cite{Kramer2004, Toftegaard2014}. As only a single field was used,
no \ac{OAR}s were considered in the optimization. The plans from $0^{\circ}$ to
$180^{\circ}$ targeted the left section of the \ac{LN}s\footnote{The seminal
vesicles were included in the LN target for \nameref{PaperRobust} and
\nameref{PaperNTCP}}, while $180^{\circ}$ to $360^{\circ}$ targeted the right.
The \ac{PTV}s were generated from the left and right \ac{LN} \ac{CTV}s by
adding an isotropic margin of $0$, $3$, $5$ and $7$ mm. The optimized fluence
maps were extracted and used to recalculate the dose on the \ac{rCT}s. The
centre of mass of the prostate was used for rigid registration.  \ac{DVH}s were
calculated for the \ac{CTV} and delineated \ac{OAR}s: Rectum, bladder and
bowel.\medskip

In \nameref{PaperRobust}, the \textit{simple} beam model of \ac{TRiP} was used
due to a large amount of data: $37$ angles per patient with four different
margins, recalculated on $7-10$ \ac{rCT}s for each of the $18$ patients. These
numbers give $\approx 37\cdot4\cdot18\cdot8 = 21312$ dose calculations to a
large lymph node target.

\subsection{Dose-guided image quality assessment (Paper III)}

Single field proton plans were created for three gantry angles, $0^{\circ}$,
$90^{\circ}$ and $180^{\circ}$. The plans were optimized to a spherical target
with a diameter of $5$ cm in the centre of the \ac{rigidCT}, using Varian
Eclipse 13.7. The optimized spot plans were recalculated on each of the
\ac{CBCT} reconstructions and the \ac{deformCT}. The dose-cubes were then
exported for gamma analysis.

\subsection{NTCP and robustness analysis (Paper II)}

In \nameref{PaperNTCP}, both proton and photon plans were created and optimized
on a \ac{PTV} generated from the prostate \ac{CTV} with a $7$ mm axial margin
and $9$ mm \ac{SI}, and the \ac{LN} \ac{CTV} with an axial and \ac{SI} margin
of $8$ and $5$, respectively. The prostate \ac{PTV} was prescribed $74$ Gy and
the \ac{LN}s $55$. The photon plans use two \ac{VMAT} arcs of $15$ MV,
$181^{\circ}$ to $179^{\circ}$ clockwise and $179^{\circ}$ to $181^{\circ}$
counter-clockwise. Two proton plans were made, one with opposing lateral fields
and one with anterior oblique fields at $35^{\circ}$ and $325^{\circ}$ based on
results from \nameref{PaperRobust}. The optimized plans were recalculated on
the \ac{rCT}s with soft-tissue based registration.

\subsection{Online range-guidance evaluation (Paper IV)}

The treatment plans for \nameref{PaperWEPL} were made with robust optimization
in the TPS, which does worst-case optimization based on $5$ mm \ac{CT} shifts
in 6 directions and a range uncertainty of $3.5\%$. The plan used five fields,
two opposing lateral fields to the prostate and \ac{SV}, and three posterior
and posterior oblique fields to the \ac{LN}s. The dose optimization criteria
were based on an in-house developed protocol (Suppl. Mat. B of
\nameref{PaperWEPL}).  The optimized spot plans were recalculated on the
\ac{rCT}s with bony-anatomy based registration.

\section{Scatter correction method (Paper III)}

The scatter correction method was based on the initial implementation by Park
et al.\cite{Park2015a}. It was further expanded to enable the use of
projections of the file formats Hnd and Xim, and subsequently with additional
features (e.g.\ the structure-based WEPL calculations), bug fixes, tests,
\ac{CI} and performance improvements.  The scatter correction algorithm took a
\ac{pCT} and \ac{CB} projections as input.  The first step after reading the
input was a regular \ac{FDK} based reconstruction to generate the \ac{rawCBCT}
using the \ac{RTK} library\cite{Rit2014}. The \ac{rawCBCT} had an image size of
$512 \times 512 \times 200$ voxels. The voxel size in the x- and y-direction
was $512$ divided by the \ac{FOV} diameter of the projection geometry. The
slice thickness was fixed at $1$ mm.\medskip

Subsequently, rigid and deformable registrations of the \ac{pCT} to the
\ac{rawCBCT} were performed to generate the \ac{rigidCT} and \ac{deformCT}. The
registrations were both performed with Plastimatch\cite{Shackleford2010a}. In
\nameref{PaperCBCT}, static phantoms were used, making the \ac{DIR} almost
redundant.  The \ac{DIR} used the \ac{MI} metric for the Alderson thorax and
pelvis and the \ac{MSE} metric for Head and Catphan as it has given better
results in this area\cite{Kim2017}.\medskip

The \ac{deformCT} were forward projected onto the \ac{CB} geometry.  The
forward-projected projections were then subtracted from the CB projections.
These residual projections were then smoothed with a Gaussian and a median
filter to simulate the scatter and the beam hardening\cite{Zollner2017}. The
simulated scatter were finally subtracted from the CB projections before then
reconstructing to the scatter corrected \ac{corrCBCT}.

\section{Statistics and data analysis methods}

\subsection{Patient clustering (Paper I)}

A k-nearest-neighbour cluster comparison analysis was performed across beam
angles \cite{Chaikh2014, Legendre2012NumEco} to identify possible
sub-populations of patients with similar patterns in motion robustness.  This
analysis compared $\log_2$ normalized WEPL differences and $\log_2$ normalized
dose degradation between patients and beam angles.  The $\log_2$ normalization
was done by $\log_2((x_i - \mu_x)/\sigma_x)$; the resulting values were
unitless.  The angles (columns) and patients (rows) were grouped and sorted by
similarity, using the Pearson k-nearest-neighbour distance.  Moreover, trees
representing the Pearson distance were generated to identify similar patients,
i.e.\ patient populations, and the robust angles for these groups
simultaneously.

\subsection{Predictive analysis (Paper I)}

In \nameref{PaperRobust}, patterns of dose degradation using fewer data, i.e.\
early on in the course of treatment, were explored.  Doses to the \ac{CTV} and
\ac{OAR}s were analyzed in two ways, with all \ac{rCT}s and only the two first
\ac{rCT}s.

\subsection{NTCP evaluation (Paper II)}

In \nameref{PaperNTCP}, \ac{gEUD} were calculated for the rectum ($a=12$) and
bladder ($a=8$)\cite{hysing_influence_2008}.  Additionally, \ac{NTCP} were
calculated for both, based on grade 2 rectal bleeding
\cite{rancati_fitting_2004} and bladder obstruction \cite{thor_urinary_2016}
endpoints, using the \ac{LKB} model \tref{tab:LKBparams}.

\input{tex/tables/LKBparams}

The dose to the overlapping region of the \ac{PTV} and the \ac{OAR} were
calculated. The \ac{NTCP} and selected \ac{DVH} values were plotted and
correlated to the overlap ratio.

\subsection{Gamma analysis (Paper III)}

For all the three treatment plans of \nameref{PaperCBCT}, the dose
distributions were compared using gamma analysis\cite{Low1998}.  The dose of
the \ac{rigidCT} was used as the truth. Only the dose above a threshold of
$10\%$ of the prescription dose was considered.  The gamma pass-rate were
calculated for three tolerances of ${1\%}/{1~\textrm{mm}}$,
${2\%}/{2~\textrm{mm}}$ and ${3\%}/{3~\textrm{mm}}$. The gamma pass-rates were
compared using the \ac{DABEST} framework\cite{ho_moving_2019} to compute and
plot the mean paired difference with $95\%$ confidence intervals and the raw
data with mean and \ac{SD}.

\subsection{Correlation of WEPL with Dose metrics (Paper IV)}

Isodoses were created using the \ac{CERR} platform to quantify \ac{WEPL}
changes in \nameref{PaperWEPL} for multiple dose levels: $15$, $20$ and $25\%$
of the prescription dose for the posterior and posterior oblique fields, and
$25$, $30$ and $35\%$ of the prescription dose for the opposed lateral
fields.\medskip

For each field's isodose of the \ac{pCT}, the distal iso-WEPL were calculated.
Then, using Hausdorff distances, the iso-WEPLs were compared against the
corresponding isodoses of the \ac{pCT} and all \ac{rCT}s. The Hausdorff
distance for all points in both directions was saved, i.e.\ $A \rightarrow B$
and $B \rightarrow A$ with $A:B$ being \ac{pCT}:iso-WEPL, \ac{rCT}:iso-WEPL and
\ac{pCT}:\ac{rCT}. For each of these, the percentage of points with a Hausdorff
distance less than $2$, $3$, $4$ and $5$ mm were extracted as a spatial
agreement measure. The mean and $95\%$ confidence interval were estimated using
a linear mixed-effect model.\medskip

The spatial agreements between the \ac{pCT} and iso-WEPL were tested as a
predictor for different dose metrics by calculating sensitivity, specificity,
\ac{PPV} and \ac{NPV}.

\section{Tools}

\subsection{Programming languages, applications, libraries etc.}

Scripts and applications were written for the methods and data analysis for all
papers and subprojects of this project.  Scripting and prototyping were mainly
done in Python (2.7 for \nameref{PaperRobust} and 3+ for everything else) and
to a lesser degree in C++. R, Python and \matl~were used for statistics and
data analysis depending on what was convenient.  Python had some benefits when
working with strings and more general-purpose computing, while R was strongest
in statistics scripting. \matl~has mainly been designed for linear algebra
calculus, although it was mainly used to visualize results.  Several small
\ac{CLI} applications and two larger \ac{UI} applications were written in C++.
C++ has the main advantage of being a compiled language with high performance
and the "foot gun"\footnote{“C makes it easy to shoot yourself in the foot; C++
makes it harder, but when you do it blows your whole leg off.” --- Bjarne
Stroustrup (creator of C++)} disadvantage.\medskip

The tools and applications used in this project were open-source software, with
the main exceptions being \matl~and \ac{CUDA} itself and the \ac{TPS}s.  The
tools included basic Linux \ac{CLI}s, in particular for formatting and
extracting information in application output. The Plastimatch
\cite{Shackleford2010a}, \ac{RTK} \cite{Rit2014}, \ac{DCMTK} and \ac{GDCM}
\ac{CLI}s. The same tools' \ac{API}s were also used, and the \ac{API}s of
\ac{ITK} \cite{mccormick_itk_2014} and Boost (\url{boost.org}) for C++. For
Python and R, a complete list of packages used is too long to include here;
suffice to say mainly the most popular packages for data analysis, such as
"numpy" and "ggplot2".\medskip

The compute-heavy parts of the scatter correction application, \ac{CBCTRecon},
were accelerated using the \ac{OpenCL} \ac{API} to utilize \ac{GPU}s and
similar highly parallel processors.  \ac{RTK} and Plastimatch were optionally
accelerated using the programming language for Nvidia \ac{GPU}s, \ac{CUDA}. The
application, therefore, ran a great deal faster with Nvidia \ac{GPU}s. The
\ac{CUDA} forward-projection from \ac{RTK} was rewritten to use \ac{OpenCL} for
\nameref{PaperCBCT}. This re-implementation made the application run
approximately equally fast on non-Nvidia hardware.\medskip

The \ac{CBCTRecon} \ac{UI} used the cross-platform Qt (\url{qt.io}) framework,
an XML and a C++ \ac{API} that generates a graphical \ac{UI}. The build system
was written in CMake, another open-source tool, for cross-platform project
configuration and finding packages installed on the system. The software was
compiled with C++17 compatible compilers, although \ac{CUDA} was only
compatible with particular compilers' specific versions.  Initially, the three
most popular desktop \ac{OS} were supported. However, macOS removed support for
\ac{OpenCL} in an attempt to move developers to their \ac{API}, Metal (
\url{developer.apple.com/macos/whats-new/}), which was macOS only. So now only
Windows and Linux are supported.

\subsection{Application testing and verification}

Automated tests were set up to run for these \ac{OS}es with different compilers
and \ac{GPU} backends using the GitLab \ac{CI} pipelines. The \ac{CI} was part
of the version control system on \url{gitlab.com}. Thus, at every code commit,
a minimal virtual machine with a Docker environment would be created on a
server, on which the source code would be downloaded and compiled. And more
importantly, automated testing of the software at every code change. The
pipeline was also configured to run on different hardware by making personal
computers with different \ac{GPU}s available through the "gitlab-runner", which
required some extra work-arounds\footnote{A few weeks before the time of
submission for this thesis, the merge request for docker-based GPU support was
accepted into the gitlab-runner open-source repository, simplifying the process
significantly
\url{gitlab.com/gitlab-org/gitlab-runner/-/merge_requests/1955}.}.

\subsection{Treatment planning systems}

Two different \ac{TPS}s were used: \ac{PyTRiP} \cite{Toftegaard2014} for
\nameref{PaperRobust} and Varian Eclipse \cite{Eclipse13} research edition for
the other papers. The \ac{PyTRiP} frontend is open source, and it was therefore
modified for our needs, while the backend, \ac{TRiP} \cite{Kramer2004}, was a
closed source binary.  \ac{TRiP} was a \ac{REPL}, and \ac{PyTRiP} communicated
with it by piping generated commands into the standard input of the \ac{TRiP}
\ac{REPL}.\medskip

The \ac{goPMC}\cite{Qin2016} was integrated into \ac{CBCTRecon} through
\ac{API} calls to a closed-source binary library. The \ac{gPMC} integration was
also planned --- although unfortunately, we prioritized other projects. These
were not technically \ac{TPS}s. We integrated these tools to make fast dose
recalculation available on scatter-corrected \ac{CBCT}s based on the actual
\ac{TPS} plans. To a large extent, the integration consisted of morphing the
\ac{TPS} generated \ac{DICOM} plan into the necessary information for dose
calculation.  The integration was tested using \ac{DICOM} files from three
different \ac{TPS} vendors, all of which behave differently, e.g.\ by choice of
coordinate systems.

The \ac{API} of \ac{goPMC} was quite simple. It  required a \ac{CT} image, a
string describing what to calculate (dose to water/medium, \ac{LET} or
fluence), number of particles, and four input vectors. The \ac{CT} must be $512
\times 512$ pixels per slice and was rescaled to $256 \times 256$ before the
dose calculation.  The vectors represent the source positions, direction,
weight and energy of the protons.

For the integration, a translator was written reading a \ac{DICOM} format
proton plan.  The spot positions were read, and particle positions were
distributed from the given weight and sigma around each spot if it was a
spot-scanning plan. If it was a passive scatter plan, the compensator
thickness, the collimator's shape, and any range modulators or shifters
upstream of the aperture were read instead. The  protons' energy was corrected
by calculating the energy loss, using the Bethe formula, over the \ac{WEPL} of
the aperture and range modifiers.  Unlike spot scanning, the spot positions
were distributed equally over the "milling tool diameter"\footnote{See
\url{dicom.nema.org/medical/dicom/2014c/output/chtml/part03/sect_C.8.8.25.html}
at (300A, 02E8)}. The weight of each energy given by the modulator wheel's
level width was calculated by the relative angle of that modulator level
width to the total rotation angle.\medskip

Three different \ac{DICOM} plans were used for testing, two with \ac{PBS} and
one with passive scatter. Each plan was created in a different \ac{TPS},
Eclipse by Varian and Syngo by Siemens, while the passive scattering plan was
from RayStation by RaySearch Laboratories.  The two \ac{PBS} files, although
standardized in the \ac{DICOM} format, still had differences. For example, they
used a different direction for the y-axis of the \ac{BEV} coordinate
system.\medskip

\ac{goPMC} was compiled with \ac{MSVC13} in debug mode.  Debug mode means, in
this case, that the compiler did not perform any optimizations on the code,
which helps track down bugs, but it also makes the application slower.
However, as the application used \ac{OpenCL}, which is just-in-time compiled by
the driver, the slowdown is probably negligible for \ac{goPMC}. Nevertheless,
this also meant that when linking against \ac{goPMC}, we also needed to use
\ac{MSVC13} in debug mode.  A small \ac{CLI} wrapper was created around the
\ac{API} to avoid compiling the entire application with that specific compiler
and mode, called from the main application. The \ac{DICOM} plan was read and
interpreted inside the \ac{CLI} to avoid unnecessarily large file \ac{IO}.
However, without the optimizations from the compiler, this meant the code
interpreting the \ac{DICOM} plan and generating the four vectors needed to be
manually optimized. This optimization was needed because there were usually
tens of thousands of spots in a plan, and the weight, position, direction and
energy needed to be calculated.  For the position and direction, the data is
3D. Thus \ac{SSE2} instructions could be used to perform the simple
arithmetic in parallel in single instructions.  \ac{OpenMP} was used to have
the loops running in parallel, utilizing as many threads simultaneously as
possible. To distribute the positions randomly in a Gaussian distribution of a
given sigma for each spot, an inverse \ac{CDF} \ac{LUT} was used. The randomly
generated number between $0.0$ and $1.0$ was compared sequentially against a
list of values rising from $0.0$ to $1.0$ of the same length as the \ac{CDF}
table, i.e.\ using it as a \ac{LUT} with nearest neighbour interpolation. This
method was fast when considering the alternative complexity of calculating the
integral or using approximated functions.\medskip

As mentioned, the application was tested on three plans. The spot scanning
plans seemed to be calculated correctly. However, the passive scattering plan
would trigger an error on 9 out of 10 runs. The randomness of Monte Carlo could
be the reason for not always getting the bug when running it multiple times
with the same input. This bug remains unsolved. No results in this thesis are
based on data from this program. However, the bug also only has two outcomes:
correct results or no results.
