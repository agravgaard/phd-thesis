[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/agravgaard/phd-thesis)

# PhD Thesis

[Plagiarism report vs master thesis](https://agravgaard.gitlab.io/phd-thesis/report.html)

[Thesis PDF](https://agravgaard.gitlab.io/phd-thesis/Thesis.pdf)

[Summary + Papers PDF](https://agravgaard.gitlab.io/phd-thesis/Short.pdf)

Original submission vs current version of the thesis:
  * [First part](https://agravgaard.gitlab.io/phd-thesis/diff_first.pdf)
  * [Middle part](https://agravgaard.gitlab.io/phd-thesis/diff_mid.pdf)
  * [Last part](https://agravgaard.gitlab.io/phd-thesis/diff_last.pdf)
  * [Suppl. Material part](https://agravgaard.gitlab.io/phd-thesis/diff_suppl.pdf)

The comparison is divided into parts for each page-drift (as the new version is longer).
